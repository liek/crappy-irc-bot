import time
import copy
import logging
from collections import defaultdict

from user import ChannelUser

logger = logging.getLogger(__name__)


class ChannelManager(object):
    def __init__(self, irc):
        self.irc = irc
        self.channels = {}

    def joining(self, channel):
        chname = self.irc.irclower(channel)
        ch = Channel(channel, self.irc)
        self.channels[chname] = ch
        return ch

    def parting(self, channel):
        chname = self.irc.irclower(channel)
        del self.channels[chname]

    def get(self, channel_name):
        # Filter statusmsg prefixes
        channel_name = channel_name.lstrip(self.irc.user_prefixes)

        channel_name = self.irc.irclower(channel_name)
        if channel_name in self.channels:
            return self.channels[channel_name]

        """silently fail -- should be garbage collected"""
        return Channel()

    def copy(self):
        cmgr = ChannelManager(self.irc)
        for ch in self.channels:
            cmgr.channels[ch] = self.channels[ch].copy()
        return cmgr


class Channel(object):
    def __init__(self, channel_name='', irc=None):
        self.users = {}
        self.time_joined = time.time()
        self.topic = ""
        self.topic_time = ""
        self.topic_setter = ""
        self.cmodes = {}
        self.getting_names = False
        self.channel = channel_name
        self.irc = irc
        self.bans = defaultdict(dict)

    def __repr__(self):
        return "<Channel %s: %s %s %s>" % (
            self.channel, self.topic, self.topic_time, self.topic_setter
        )

    def __str__(self):
        return self.channel

    def process_who(self, data):
        if not data:
            return

        nick = data[5]
        username = data[2]
        host = data[3]
        modes = {self.irc.prefix_to_mode(p) for p in list(data[6][1:])}

        self.add_name(nick, username, host, modes)

    def names(self):
        return [u.nick for u in self.users.values()]

    def process_names(self, sender, data):
        if self.getting_names:
            for name in data.split():
                idx = 0
                modes = set()
                for c in name:
                    mode = self.irc.prefix_to_mode(c)
                    if not mode:
                        break
                    modes.add(mode)
                    idx += 1

                self.add_name(name[idx:], mode=modes)

    def start_names(self):
        self.getting_names = True

    def get_user(self, name):
        lcnick = self.irc.irclower(name)
        return self.users.get(lcnick, None)

    def add_name(self, name, username=None, host=None, mode=set()):
        user = self.users.get(name, None)
        if not user:
            user = ChannelUser(name, modes=mode)
            self.users[self.irc.irclower(name)] = user

        if username:
            user.username = username

        if host:
            user.host = host

    def remove_name(self, name):
        lcnick = self.irc.irclower(name)
        try:
            del self.users[lcnick]
        except KeyError:
            pass

    def change_name(self, old_name, new_name):
        old_lcnick = self.irc.irclower(old_name)
        new_lcnick = self.irc.irclower(new_name)
        try:
            user = self.users[old_lcnick]
            del self.users[old_lcnick]
            self.users[new_lcnick] = user
            user.nick = new_name
        except KeyError:
            pass

    def change_mode(self, type, mode, data, setter=None):
        list_modes = self.irc.support['chanmodes']['lists']
        if type == "+":
            if mode in self.irc.user_flags:
                try:
                    self.get_user(data).add_mode(mode)
                except AttributeError:
                    pass
            elif mode in list_modes:
                self.add_ban(mode, data, setter, time.time())
            else:
                self.cmodes[mode] = data
        else:
            if mode in self.irc.user_flags:
                try:
                    self.get_user(data).remove_mode(mode)
                except AttributeError:
                    pass
            elif mode in list_modes:
                self.remove_ban(mode, data)
            else:
                try:
                    del self.cmodes[mode]
                except KeyError:
                    pass

    def ops(self):
        return [u.nick for u in self.users.values() if u.isop()]

    def voices(self):
        return [u.nick for u in self.users.values() if u.isvoice()]

    def end_names(self):
        self.getting_names = False

    def isop(self, name):
        try:
            return self.get_user(name).isop()
        except AttributeError:
            return False

    def isvoice(self, name):
        try:
            return self.get_user(name).isvoice()
        except AttributeError:
            return False

    def add_ban(self, bantype, mask, setter, when):
        self.bans[bantype][mask] = {'setter': setter, 'when': when}

    def remove_ban(self, bantype, mask):
        try:
            del self.bans[bantype][mask]
        except KeyError:
            pass

    def get_bans(self, bantype='b'):
        try:
            return self.bans[bantype]
        except KeyError:
            return {}

    def limit(self):
        if 'l' in self.cmodes:
            return int(self.cmodes['l'])
        else:
            return None

    def key(self):
        if 'k' in self.cmodes:
            return self.cmodes['k']
        else:
            return None

    def copy(self):
        chan = copy.copy(self)
        users = {}
        for n in chan.users:
            users[n] = chan.users[n].copy()
        chan.users = users
        chan.bans = copy.deepcopy(chan.bans)
        chan.cmodes = copy.deepcopy(chan.cmodes)
        return chan
