$(function () {
    var options = {
        target: '#output',
        type: 'POST',
        url: ''
    };

    $('form').submit(function () {
        var url = $(this).attr('action');
        var cmd = $('input[name=cmd]').val();

        options.url = url + cmd;
        //alert(options.url);

        $(this).ajaxSubmit(options);

        $('input[type=text]').val('');

        return false;
    });
});
