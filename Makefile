start:
	./cib.py start
stop:
	./cib.py stop
restart: stop start
clean:
	rm -rf ./plugin/*.pyc ./*.pyc
container:
	docker build -t crappy-irc-bot -f infra/Dockerfile .
