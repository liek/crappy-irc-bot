import re
import string  # for nick randomization on collision
import random  # for nick randomization on collision
import time
import threading
from socket import gethostname
from collections import deque, OrderedDict
from datetime import datetime, timedelta
import copy
import queue
import logging

from connection import Connection
from user import User
import util

VERSION = 0, 0, 1
DEBUG = 1

logger = logging.getLogger(__name__)


class Protocol(threading.Thread):
    case_mappings = {
        'rfc1459': {'lower': "[\\]^", 'upper': "{|}~"},
        'strict-rfc1459': {'lower': "[\\]", 'upper': "{|}"},
        'ascii': {'lower': "", 'upper': ""},
    }

    def connect(self, ircserver, port, nicknames, max_nick_tries=10,
                password=None, username=None, ircname=None,
                version="crappy irc", debug=False, source_addr=None):
        if self.connected:
            return False

        self._reset()
        self.server = ircserver
        self.port = port
        self.password = password
        self.username = username
        self.ircname = ircname
        self.source_address = source_addr
        self.version = version

        self.set_loglevel(debug)

        self.nicknames = nicknames[:]
        self.max_nick_tries = max_nick_tries
        self.nick_changes = 0
        self.nick_idx = 0

        self.userinfo = User(self.nicknames[0], self.username, self.localhost)

        if not self._conn.connect(self.server, self.port, self.source_address):
            logger.info("Could not connect to host")
            self.connected = False
            return False

        logger.info("Connected to %s" % (self.server))
        self.connected = True

        self.send("CAP REQ :multi-prefix")
        self.send("CAP END")

        # USER/PASS can't be sent after registration, no need for public cmds
        if self.password:
            self.send("PASS %s" % self.password)

        self.send("USER %s 0 * :%s" % (username, ircname))
        self.nick(self.nicknames.pop(0))

        return True

    def _reset(self):
        self.data = b""
        self.connected = False
        self.registered = False
        self.localhost = gethostname()
        self.user_flags = "ov"
        self.user_prefixes = "@+"
        self.user_flag_map = OrderedDict({'o': '@', 'v': '+'})
        self.user_prefix_map = OrderedDict({'@': 'o', '+': 'v'})
        self.shutting_down = False
        self.registered = False
        self.set_flags = "bkl"
        self.unset_flags = "bk"
        self.support = {
            'chanmodes': {'lists': 'b',
                          'always': 'k',
                          'when_set': 'l',
                          'no_param': 'imnpst'},
            'chantypes': '#&',
        }

        self._user_modes = set()

        self.userinfo = User('unknown', 'unknown', 'localhost')

        self.sendqueue = deque()
        self.sendqueue_last = 0
        self.sendqueue_wait = 0

        # Start queuing once our time >= current time + sendqueue_delay
        self.sendqueue_delay = 10
        # Penalty for each message sent in seconds
        self.sendqueue_penalty = 2

        # How often to PING server when we haven't received data
        self.timeout_interval = 90
        # Assume connection is dead after timeout_wait
        self.timeout_wait = 30
        self.last_recv = time.time()
        self.last_ping = 0
        self.check_timeout = False
        self._set_case_mapping('rfc1459')

    def __init__(self):
        threading.Thread.__init__(self)
        self._queue = queue.Queue()
        self._conn = Connection(self.recv, self.disconnect_handler)
        self._scheduled_calls = []
        self._scheduler_mutex = threading.RLock()
        self._sendqueue_mutex = threading.RLock()

        self._reset()

    def copy_state(self):
        irc = copy.copy(self)
        irc.chmgr = self.chmgr.copy()
        irc.userinfo = copy.copy(self.userinfo)
        return irc

    def set_loglevel(self, debug):
        self.debug = debug
        if self.debug:
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)

    def send(self, message):
        """Internal function to send data through the socket.

        Parameters:
        message -- Raw IRC message
        """
        if not self.connected:
            return
        with self._sendqueue_mutex:
            curtime = time.time()
            if self.sendqueue_wait < curtime:
                self.sendqueue_wait = curtime

            if self.sendqueue or \
                round(self.sendqueue_wait - curtime, 2) \
                    >= self.sendqueue_delay:
                self.sendqueue.append(message)
            else:
                self.send_immediate(message)

    def send_immediate(self, message):
        """Sends data to server bypassing send queue.

        Parameters:
        message -- Raw IRC message
        """
        if not self.connected:
            return
        with self._sendqueue_mutex:
            curtime = time.time()
            if self.sendqueue_wait < curtime:
                self.sendqueue_wait = curtime

            self._send_data(message)
            self.sendqueue_wait += self.sendqueue_penalty

    def _send_data(self, message):
        """Internal function to send data to server

        Parameters:
        message -- Raw IRC message
        """
        logger.debug(">>> %s" % (message))
        self._conn.send(util.encode(message + "\r\n"))
        self.sendqueue_last = time.time()

    def process_sendqueue(self):
        """
        Internal function for processing the send buffer, to prevent flooding.

        Parameters:
        None
        """
        with self._sendqueue_mutex:
            if not self.sendqueue:
                return
            curtime = time.time()

            if (curtime - self.sendqueue_last) >= self.sendqueue_penalty:
                while 1:
                    msg = self.sendqueue.popleft()
                    self._send_data(msg)
                    self.sendqueue_wait += self.sendqueue_penalty
                    if not self.sendqueue:
                        break
                    if round(self.sendqueue_wait - curtime, 2) >= \
                            self.sendqueue_delay:
                        break

    def nick(self, nickname):
        """Change/Set IRC Nickname

        Parameters:
        nickname -- Nick to change to
        """
        self.send("NICK %s" % nickname)

    def nextnick(self):
        if self.nick_changes < self.max_nick_tries:
            if self.nick_idx < len(self.nicknames):
                self.nick(self.nicknames[self.nick_idx])
            else:
                nick_base = self.nicknames[self.nick_idx % len(self.nicknames)]
                self.nick(nick_base + "-" +
                          ''.join(random.choice(
                              string.ascii_lowercase + string.digits)
                              for _ in range(2)))
        else:
            self.disconnect("")
        self.nick_idx += 1

    def quit(self, message):
        """Send QUIT message. Use the disconnect() method instead.

        Parameters:
        message -- Optional quit message
        """
        self.send_immediate("QUIT%s" % ((" :%s" % message) if message else ""))

    def disconnect(self, message):
        """Close socket and send QUIT.

        Parameters:
        message -- Quit message
        """
        if not self.connected:
            return
        self.quit(message)
        self._conn.disconnect()

    def terminate(self, message):
        """Disconnect and stop reconnects

        Parameters:
        message -- Quit message
        """
        self._queue.put(('shutdown', message))

    def _terminate(self, message):
        self.shutting_down = True
        self.disconnect(message)

    def disconnect_handler(self, msg):
        self.connected = False
        if self.shutting_down:
            self.alive = False

        self._queue.put(('disconnect', msg))

    def recv(self, data):
        """This function will be called from the recieve thread, and will
        decode the incoming data and put it in the main queue"""

        """Unfortunely we can't use regex on bytearrays, but we have to split
        before decoding to prevent it from choking if we receive mixed utf-8
        and windows-1252 messages :/
        """
        lines = (self.data + data).split(b'\n')
        self.data = lines[-1]
        lines = lines[:-1]
        self.last_recv = time.time()
        for line in lines:
            line = util.decode(line).rstrip('\r')
            logger.debug("<<< %s" % line)
            if not line:
                continue
            self.process_data(line)
        return ""

    def process_data(self, data):
        line = data

        if line is None:
            return
        if line.startswith(':'):
            try:
                source, line = line[1:].split(' ', 1)
            except ValueError:
                logger.debug("Empty message from server: %s" % line)
                return
        else:
            source = None
        argstr, part, text = line.partition(' :')
        args = argstr.split()
        if part:
            args.append(text)
        sender = User.from_hostmask(source)
        try:
            command = args.pop(0)
        except IndexError:
            logger.debug("Empty message from server: %s" % line)
            return
        self._queue.put(('irc', sender, command, tuple(args)))

    def pre_dispatch(self, sender, command, args):
        """dispatch() must call this before handing off to plugins. This will
        update state data (i.e. creating channels), as well as labeling the
        arguments (returns an updated argument list/event name)"""
        handler = getattr(self, '_handle_' + command.lower(),
                          self._unhandled_cmd)
        return handler(sender, command, args)

    def _unhandled_cmd(self, sender, command, args):
        return command, {'args': args}

    def argcheck(req, fill=0, fill_with=""):
        def check(f):
            def handler(self, sender, command, args):
                if len(args) >= req:
                    if len(args) < fill:
                        fargs = [fill_with for _ in range(fill - len(args))]
                        args = tuple(args + tuple(fargs))
                    return f(self, sender, command, args)
                else:
                    return command, {'args': args}
            return handler
        return check

    @argcheck(1)
    def _handle_ping(self, sender, command, args):
        kwargs = {'message': args[-1]}
        self.send_immediate("PONG :%s" % kwargs['message'])
        return 'ping', kwargs

    @argcheck(1)
    def _handle_pong(self, sender, command, args):
        kwargs = {'message': args[-1]}
        self.check_timeout = False
        return 'pong', kwargs

    @argcheck(1)
    def _handle_001(self, sender, command, args):  # RPL_WELCOME
        self.registered = True
        logger.info("001 handled -- connection authorized")
        self.nickname = args[0]
        self.userinfo.nick = args[0]

        channels = []
        keys = []
        for ch in self.channels:
            try:
                """channels with keys need to come first"""
                chan, key = ch.split(':')
                channels.insert(0, chan)
                keys.insert(0, key)
            except:
                chan = ch
                channels.append(chan)

        self.joinc(",".join(channels), ",".join(keys))
        return 'rpl_welcome', {'args': args}

    @argcheck(1)
    def _handle_005(self, sender, command, args):  # RPL_ISUPPORT
        """If we follow the protocol, then we are getting notice that we
        need to connect to a different server. If we follow convention, we
        are getting server info."""
        self._parse_isupport(args)
        return 'rpl_isupport', {'args': args}

    @argcheck(1)
    def _handle_nick(self, sender, command, args):
        kwargs = {'newnick': args[-1]}
        for ch in self.chmgr.channels:
            self.chmgr.get(ch).change_name(sender.nick, kwargs['newnick'])
        return 'nick', kwargs

    @argcheck(2)
    def _handle_mode(self, sender, command, args):
        if self.is_channel(args[0]):
            kwargs = {'channel': args[0], 'modes': args[1], 'params': args[2:]}
            ch = self.chmgr.get(kwargs['channel'])
            modes = self._parse_modes(args[1], args[2:])
            for mode in modes:
                ch.change_mode(mode[0], mode[1], mode[2], sender)
            return 'mode', kwargs
        else:
            kwargs = {'modes': args[1]}
            self._change_usermodes(args[1])
            return 'usermode', kwargs

    def _parse_modes(self, modes, mode_params):
        rdata = []
        params = deque(mode_params)

        if modes[0] not in "+-":
            logger.info("Invalid MODE setting: %s %s" %
                        (modes, ' '.join(mode_params)))
            return []

        for i in modes:
            if i == '-':
                mflags = self.unset_flags + self.user_flags
                type = '-'
            elif i == '+':
                mflags = self.set_flags + self.user_flags
                type = '+'
            elif i in mflags:
                try:
                    rdata.append((type, i, params.popleft()))
                except IndexError:
                    logger.info('Not enough parameters in MODE from server.')
                    rdata.append((type, i, None))
            else:
                rdata.append((type, i, None))
        return rdata

    def _change_usermodes(self, modes):
        if len(modes) < 2 or modes[0] not in '+-':
            return

        for m in modes:
            if m in '+-':
                type = m
            elif type == '+':
                self._user_modes.add(m)
            elif type == '-':
                self._user_modes.discard(m)

    @argcheck(1)
    def _handle_join(self, sender, command, args):
        kwargs = {'channel': args[-1]}
        if sender.nick == self.nickname:
            self.userinfo = sender
            channel = self.chmgr.joining(kwargs['channel'])
            channel.start_names()
            self.who(kwargs['channel'])
        else:
            chan = self.chmgr.get(kwargs['channel'])
            chan.add_name(sender.nick, sender.username, sender.host)
        return 'join', kwargs

    m_userhost = re.compile(r'([^\*=]+)(\*)?=([\+\-])([^@]+)@(.*)')

    @argcheck(1)
    def _handle_302(self, sender, command, args):  # RPL_USERHOST
        user_strs = args[-1].split()
        users = []
        for u in user_strs:
            m = self.m_userhost.match(u)
            if m:
                nick, oper, away, user, host = m.groups()
                users.append({'nick': nick, 'oper': oper == '*',
                              'away': away == '-', 'user': user, 'host': host})
        return 'rpl_userhost', {'users': users}

    @argcheck(2)
    def _handle_324(self, sender, command, args):  # RPL_CHANNELMODEIS
        kwargs = {'channel': args[1], 'modes': args[2], 'params': args[3:]}
        chan = self.chmgr.get(kwargs['channel'])
        modes = self._parse_modes(kwargs['modes'], kwargs['params'])
        for mode in modes:
            chan.change_mode(mode[0], mode[1], mode[2])

        if not chan.bans:
            bflags = 'b'
            if 'e' in self.set_flags:
                bflags += 'e'
            if 'I' in self.set_flags:
                bflags += 'I'
            self.send('MODE %s %s' % (kwargs['channel'], bflags))
        return 'rpl_channelmodeis', kwargs

    @argcheck(1, 2)
    def _handle_332(self, sender, command, args):  # RPL_TOPIC
        kwargs = {'channel': args[1], 'topic': args[-1]}
        chan = self.chmgr.get(kwargs['channel'])
        chan.topic = kwargs['topic']
        return 'rpl_topic', kwargs

    @argcheck(3, 4)
    def _handle_333(self, sender, command, args):  # RPL_TOPICSETBY
        kwargs = {'channel': args[1], 'when': args[3], 'setter': args[2]}
        chan = self.chmgr.get(kwargs['channel'])
        chan.topic_time = kwargs['when']
        chan.topic_setter = kwargs['setter']
        return 'rpl_topicsetby', kwargs

    @argcheck(8)
    def _handle_352(self, sender, command, args):  # RPL_WHOREPLY
        try:
            hop_count, realname = args[7].split(' ', 1)
        except ValueError:
            hop_count = ""
            realname = args[7]

        kwargs = {'channel': args[1],
                  'username': args[2],
                  'host': args[3],
                  'server': args[4],
                  'nick': args[5],
                  'modes': args[6],
                  'hopcount': hop_count,
                  'realname': realname,
                  }
        self.chmgr.get(kwargs['channel']).process_who(args)
        return 'rpl_whoreply', kwargs

    @argcheck(4)
    def _handle_353(self, sender, command, args):  # RPL_NAMREPLY
        kwargs = {'chtype': args[1], 'channel': args[2], 'names': args[3]}
        chan = self.chmgr.get(kwargs['channel'])
        chan.process_names(sender, kwargs['names'])
        return 'rpl_namreply', kwargs

    @argcheck(2, 3)
    def _handle_366(self, sender, command, args):  # RPL_ENDOFNAMES
        kwargs = {'channel': args[1], 'message': args[-1]}
        self.chmgr.get(kwargs['channel']).end_names()
        self.send("MODE %s" % (kwargs['channel']))
        return 'rpl_endofnames', kwargs

    @argcheck(3, 5)
    def _handle_346(self, sender, command, args):  # RPL_INVITELIST
        kwargs = {'channel': args[1], 'mask': args[2], 'setter': args[3],
                  'when': args[4]}
        chan = self.chmgr.get(kwargs['channel'])
        chan.add_ban('I', kwargs['mask'], kwargs['setter'], kwargs['when'])
        return 'rpl_invitelist', kwargs

    @argcheck(2, 3)
    def _handle_347(self, sender, command, args):  # RPL_ENDOFINVITELIST
        kwargs = {'channel': args[1], 'message': args[-1]}
        return 'rpl_endofinvitelist', kwargs

    @argcheck(3, 5)
    def _handle_348(self, sender, command, args):  # RPL_EXCEPTIONLIST
        kwargs = {'channel': args[1], 'mask': args[2], 'setter': args[3],
                  'when': args[4]}
        chan = self.chmgr.get(kwargs['channel'])
        chan.add_ban('e', kwargs['mask'], kwargs['setter'], kwargs['when'])
        return 'rpl_exceptionlist', kwargs

    @argcheck(2, 3)
    def _handle_349(self, sender, command, args):  # RPL_ENDOFEXCEPTIONLIST
        kwargs = {'channel': args[1], 'message': args[-1]}
        return 'rpl_endofexceptionlist', kwargs

    @argcheck(3, 5)
    def _handle_367(self, sender, command, args):  # RPL_BANLIST
        kwargs = {'channel': args[1], 'mask': args[2], 'setter': args[3],
                  'when': args[4]}
        chan = self.chmgr.get(kwargs['channel'])
        chan.add_ban('b', kwargs['mask'], kwargs['setter'], kwargs['when'])
        return 'rpl_banlist', kwargs

    @argcheck(2, 3)
    def _handle_368(self, sender, command, args):  # RPL_ENDOFBANLIST
        kwargs = {'channel': args[1], 'message': args[-1]}
        return 'rpl_endofbanlist', kwargs

    @argcheck(1, 2)
    def _handle_privmsg(self, sender, command, args):
        if len(args[1]) > 1 and args[1][0] == '\001' and args[1][-1] == '\001':
            return self.__handle_ctcp(sender, command, args)
        kwargs = {'message': args[1]}
        msgtype = 'privmsg'
        if self.is_channel(args[0]) or self.prefix_to_mode(args[0][0]):
            # TODO: Separate events for statusmsgs?
            msgtype = 'pubmsg'
            kwargs['channel'] = args[0]
        return msgtype, kwargs

    def __handle_ctcp(self, sender, command, args):
        msgtype = 'ctcp'
        msg = args[1][1:-1]
        kwargs = {}
        ctcp_cmd, ctcp_data = msg.partition(' ')[::2]
        if ctcp_cmd.upper() == 'ACTION':
            if self.is_channel(args[0]) or self.prefix_to_mode(args[0][0]):
                msgtype = 'pubaction'
                kwargs['channel'] = args[0]
            else:
                msgtype = 'action'
            kwargs['message'] = ctcp_data
        else:
            kwargs['target'] = args[0]
            kwargs['command'] = ctcp_cmd
            kwargs['data'] = ctcp_data
        return msgtype, kwargs

    @argcheck(1, 2)
    def _handle_notice(self, sender, command, args):
        if len(args[1]) > 1 and args[1][0] == '\001' and args[1][-1] == '\001':
            return self.__handle_ctcpreply(sender, command, args)
        kwargs = {'message': args[1]}
        msgtype = 'notice'
        if self.is_channel(args[0]) or self.prefix_to_mode(args[0][0]):
            msgtype = 'pubnotice'
            kwargs['channel'] = args[0]
        return msgtype, kwargs

    def __handle_ctcpreply(self, sender, command, args):
        msgtype = 'ctcpreply'
        msg = args[1][1:-1]
        kwargs = {}
        ctcp_cmd, ctcp_data = msg.partition(' ')[::2]
        kwargs['target'] = args[0]
        kwargs['command'] = ctcp_cmd
        kwargs['data'] = ctcp_data
        return msgtype, kwargs

    @argcheck(1, 2)
    def _handle_topic(self, sender, command, args):
        return 'topic', {'channel': args[0], 'topic': args[1]}

    @argcheck(1, 2)
    def _handle_part(self, sender, command, args):
        return 'part', {'channel': args[0], 'message': args[1]}

    @argcheck(0, 1)
    def _handle_quit(self, sender, command, args):
        return 'quit', {'message': args[0]}

    @argcheck(2, 3)
    def _handle_kick(self, sender, command, args):
        return 'kick', {'channel': args[0],
                        'nick': args[1],
                        'message': args[2]}

    @argcheck(1, 2)
    def _handle_433(self, sender, command, args):  # ERR_NICKNAMEINUSE
        if not self.registered:
            self.nextnick()
        return command, {'args': args}

    # 437 is ERR_UNAVAILRESOURCE on EFNet at least
    # We don't want to cycle nicks if we're already registered
    _handle_437 = _handle_433

    def dispatch(self, sender, command, args):
        """This needs to be overriden by the subclass"""
        event, kwargs = self.pre_dispatch(sender, command, args)
        kwargs['sender'] = sender
        self.post_dispatch(event, kwargs)

    def add_int(self, state, command, args):
        """Adds a locally generated event"""
        self._queue.put(('int', state, command, args))

    def post_dispatch(self, event, kwargs):
        h = '_post_handle_' + event
        if hasattr(self, h):
            getattr(self, h)(**kwargs)

    def _post_handle_topic(self, sender, channel, topic):
        chan = self.chmgr.get(channel)
        chan.topic = topic
        chan.topic_time = time.time()
        if sender.host and sender.username:
            chan.topic_setter = "%s!%s@%s" % (sender.nick,
                                              sender.username,
                                              sender.host)
        else:
            chan.topic_setter = sender.nick

    def _post_handle_nick(self, sender, newnick):
        if sender.nick == self.nickname:
            self.nickname = newnick
            self.userinfo.nick = newnick

    def _post_handle_part(self, sender, channel, message):
        if sender.nick == self.nickname:
            self.chmgr.parting(channel)
        else:
            self.chmgr.get(channel).remove_name(sender.nick)

    def _post_handle_quit(self, sender, message):
        for ch in self.chmgr.channels:
            self.chmgr.get(ch).remove_name(sender.nick)

    def _post_handle_kick(self, sender, channel, nick, message):
        if nick == self.nickname:
            self.chmgr.parting(channel)
        else:
            self.chmgr.get(channel).remove_name(nick)

    def _post_handle_ctcp(self, sender, target, command, data):
        """XXX: This doesn't belong here however the logging plugin will
        show the events out of order otherwise, we need a better system to
        queue event handler priority in the future."""
        if len(command) > 1:
            if bool(re.match('VERSION', command, re.I)):
                self.ctcp_reply(sender.nick, "VERSION", self.version)
            elif bool(re.match('PING', command, re.I)):
                self.ctcp_reply(sender.nick, command, data)
            elif bool(re.match('TIME', command, re.I)):
                self.ctcp_reply(sender.nick, "TIME",
                                (time.strftime("%a %b %d %H:%M:%S %Y",
                                 time.localtime())))

    def dispatch_disconnect(self):
        """Called to inform plugins/etc the bot has disconnected."""
        pass

    def run(self):
        self.alive = True
        self.connect()
        while self.alive:
            timeout = 0.5
            if self._scheduled_calls:
                timeout = min(timeout, self._scheduled_calls[0].timeout())
            msg = None
            try:
                msg = self._queue.get(True, timeout)
            except queue.Empty:
                pass
            if msg:
                msgtype = msg[0]
                if not self.process_msg(msgtype, msg[1:]):
                    break
            self.timeout_check()
            self.scheduler_check()
            self.process_sendqueue()

    def process_msg(self, msgtype, args):
        if msgtype == 'irc':
            self.dispatch(*(args))
        if msgtype == 'int':
            argv = args[1:]
            args[0].int_dispatch(*argv)
        elif msgtype == 'shutdown':
            logger.info("Shutdown - %s" % (args[0]))
            self._terminate(args[0])
            if not self.connected:
                return False
        elif msgtype == 'disconnect':
            # Make sure send/recv threads have stopped
            self._conn.disconnect()
            self.dispatch_disconnect()
            if not self.shutting_down:
                self.reconnect()
            else:
                return False
        elif msgtype == 'reload':
            self.reload_config()
        return True

    def timeout_check(self):
        if not self.connected:
            return
        if self.check_timeout and \
                time.time() - self.last_ping >= self.timeout_wait:
            self.disconnect("Lost connection")
        if not self.check_timeout and \
                time.time() - self.last_recv >= self.timeout_interval:
            self.check_timeout = True
            self.last_ping = time.time()
            self.send_immediate("PING :TIMEOUTCHECK")

    def scheduler_check(self):
        state = None
        with self._scheduler_mutex:
            while self._scheduled_calls:
                sc = self._scheduled_calls[0]
                if sc.due():
                    sc = self._scheduled_calls.pop(0)
                    try:
                        if state is None:
                            state = self.copy_state()
                        if sc.plugin:
                            sc.plugin.add('scheduler', state, {'sch': sc})
                        else:
                            if sc.execute():
                                self._schedule(sc)
                    except:
                        logger.exception("Error in scheduled call")
                        pass
                else:
                    break

    def is_channel(self, string):
        return string and string[0] in self.support['chantypes']

    def usermodes(self):
        return ''.join(self._user_modes)

    def _parse_isupport(self, args):
        data = args[1:-1]

        for item in data:
            value = True
            if "=" in item:
                item, value = item.split("=", 1)

            if item == 'CHANTYPES':
                if not value or value is True:
                    logger.info('No parameter for CHANTYPES setting, using '
                                'default.')
                    value = '#&'

            elif item == "PREFIX":
                if value and value is not True:
                    m = re.match('\((\w+)\)(.+)', value)
                    if m and len(m.groups()[0]) == len(m.groups()[1]):
                        modes, prefixes = m.groups()
                        self.user_flag_map = OrderedDict()
                        self.user_prefix_map = OrderedDict()
                        self.user_flags = modes
                        self.user_prefixes = prefixes
                        for flag, prefix in zip(modes, prefixes):
                            self.user_flag_map[flag] = prefix
                            self.user_prefix_map[prefix] = flag
                    else:
                        logger.info('Received malformed PREFIX setting: %s' %
                                    (value))
                        continue
                else:
                    logger.info('Received empty PREFIX setting, using '
                                'default.')
                    continue

            elif item == "CASEMAPPING":
                if value in self.case_mappings.keys():
                    self._set_case_mapping(value)
                else:
                    logger.info('Received invalid CASEMAPPING setting: %s' %
                                (str(value)))

            elif item == "CHANMODES":
                try:
                    (a, b, c, d) = value.split(',')
                except ValueError:
                    logger.info('Recieved malformed CHANMODES setting: %s' %
                                (value))
                    continue

                value = {
                    'lists': a,
                    'always': b,
                    'when_set': c,
                    'no_param': d}
                self.set_flags = a + b + c
                self.unset_flags = a + b

            self.support[item.lower()] = value

    def _set_case_mapping(self, ctype):
        self.transtbl_upper = dict(zip(
            map(ord,
                string.ascii_lowercase + self.case_mappings[ctype]['lower']),
            map(ord,
                string.ascii_uppercase + self.case_mappings[ctype]['upper'])
        ))
        self.transtbl_lower = {v: k for k, v in self.transtbl_upper.items()}
        self.case_mapping = ctype

    """Yar, here be output methods"""

    def joinc(self, channel, key=""):
        """Join a channel.

        Parameters:
        channel -- Channel name
        key -- Channel key
        """

        # channel and key match string set up as "channel, channel1" and
        # "channel,channel1"
        channels = channel.replace(" ", "")
        keys = key.replace(" ", "")
        return self.send("JOIN %s%s" % (channels, keys and (" " + keys) or ""))

    def privmsg(self, who, what):
        """Send a message.

        Parameters:
        who -- Channel or nick
        what -- Message
        """

        return self.send("PRIVMSG %s :%s" % (who, what))

    def kick(self, channel, nick, reason=None):
        """Kick someone from a channel.

        Parameters:
        channel -- Channel from which to kick someone
        nick -- User to kick
        reason -- Reason for kick. Optional (default: "kicked")
        """

        return self.send("KICK %s %s" % (channel, nick) +
                         (" :%s" % (reason) if reason else ""))

    def action(self, target, action):
        """Send 'ACTION' to channel or user. AKA, /me

        Parameters:
        target -- User or channel
        action -- Text to send
        """
        return self.ctcp("ACTION", target, action)

    def ctcp(self, target, ctcptype, parameter=""):
        """Send a CTCP message.

        Parameters:
        target -- User or channel
        ctcptype -- Which CTCP command to send
        parameter -- Data for CTCP command
        """
        ctcptype = ctcptype.upper()
        return self.privmsg(
            target, "\001%s%s\001" %
            (ctcptype, parameter and (" " + parameter) or ""))

    def notice(self, target, text):
        """Send a NOTICE message.

        Parameters:
        target -- Channel or User
        text -- Message
        """
        return self.send("NOTICE %s :%s" % (target, text))

    def whois(self, target):
        """Send a WHOIS request

        Parameters:
        target -- User to lookup
        """
        return self.send("WHOIS ,%s" % (target))

    def mode(self, target, mode, params=None):
        """Set a user or channel mode.

        Parameters:
        target -- Channel or user
        mode -- Mode string
        params -- Positional parameters for mode string
        """
        return self.send("MODE %s %s %s" % (target, mode,
                                            params if params else ""))

    def usermode(self, target, mode):
        return self.send("MODE %s %s" % (target, mode))

    def who(self, target):
        """Send a WHO request

        Parameters:
        target -- User or channel to lookup
        """
        return self.send("WHO %s" % (target))

    def ctcp_reply(self, target, ctcptype, parameter):
        """Send a reply to CTCP Request.

        Parameters:
        target -- Channel or user
        ctcptype -- Command we're replying to
        parameter -- Reply data
        """
        return self.notice(target, "\001%s %s\001" % (ctcptype, parameter))

    def topic(self, channel, topic=None):
        """Get/Set channel topic.

        Parameters:
        channel -- Channel in which to set topic
        topic -- Topic to set. Optional (default: None)
        """
        return self.send("TOPIC %s%s" % (channel,
                                         (" :%s" % topic) if topic is not None
                                         else ""))

    def operator(self, user, password):
        return self.send("OPER %s %s" % (user, password))

    def names(self, channel=None):
        """Request name-list.

        Parameters:
        channel -- List of channels from which to request names.
        """
        #expects a list of channels ["#channel1", "channel2"]
        return self.send("NAMES%s" % (channel and
                         (" " + ",".join(channel) or "")))

    def list(self, channel=None, target=""):
        """Request channel list.

        Parameters:
        channel -- List of wildcard queries
        target -- ??
        """
        #expects a list of channel
        return self.send(
            "LIST%s%s" %
            (channel and (" " + ",".join(channel)) or "",
                target and(" " + target)))

    def part(self, channel, message=None):
        """Leave a channel.

        Parameters:
        channel -- Channel to leave
        message -- Parting message. Optional
        """
        channels = channel.replace(" ", "")
        return self.send("PART %s" % (channels) +
                         (" :%s" % message if message else ""))

    def userhost(self, *nicks):
        """Request user@host for users

        Parameters:
        *nicks -- Nickname(s)
        """
        return self.send("USERHOST " + ' '.join(nicks))

    def irccmp(self, str1, str2):
        """Compare two strings case-independently using the IRC servers
        CASEMAPPING setting

        Parameters:
        str1, str2 -- Strings to compare
        """
        s1 = self.irclower(str1)
        s2 = self.irclower(str2)
        return (s1 > s2) - (s1 < s2)

    def irclower(self, text):
        """Convert a string to lower-case using the IRC Servers
        CASEMAPPING setting

        Parameters:
        text -- String to convert
        """
        return text.translate(self.transtbl_lower)

    def ircupper(self, text):
        """Convert a string to upper-case using the IRC servers
        CASEMAPPING setting

        Parameters:
        text -- String to convert
        """
        return text.translate(self.transtbl_upper)

    def prefix_to_mode(self, char):
        """Checks of a character is a prefix (in a NAMES reply)

        Parameters:
        char - Character to check

        Returns the mode character (i.e. 'o' for '@')
        """
        return self.user_prefix_map.get(char, None)

    def mode_to_prefix(self, char):
        """Checks of a character is a mode that sets a flag on a user in a
        channel

        Parameters:
        char - Character to check

        Returns the prefix character (i.e. '@' for 'o')
        """
        return self.user_flag_map.get(char, None)

    def schedule(self, when, function, plugin=None, args=[], kwargs={}):
        """Schedules a function to be called later.

        Parameters:
        when - When to execute function, either a datetime object or an amount
        of seconds from now
        function - Function to call
        args, kwargs - Arguments to pass to function
        """
        sc = ScheduledCall(when=when, function=function, plugin=plugin,
                           args=args, kwargs=kwargs)
        self._schedule(sc)

    def schedule_periodic(self, interval, function, when=None, plugin=None,
                          args=[], kwargs={}):
        """Schedules a function to be called repeatedly

        Parameters:
        interval - Interval between each call, either a timedelta
        representation or an amount of seconds
        function - Function to call
        when - When to execute function the first time. This is calculated from
        the interval if None
        args, kwargs - Arguments to pass to function
        """
        sc = PeriodicScheduledCall(when=when, function=function,
                                   args=args, kwargs=kwargs, interval=interval,
                                   plugin=plugin)
        self._schedule(sc)

    def _schedule(self, sch):
        with self._scheduler_mutex:
            inserted = False
            for i in reversed(range(len(self._scheduled_calls))):
                if self._scheduled_calls[i].when() <= sch.when():
                    self._scheduled_calls.insert(i + 1, sch)
                    inserted = True
                    break
            if not inserted:
                self._scheduled_calls.insert(0, sch)

    def scheduler_clear(self, plugin):
        with self._scheduler_mutex:
            self._scheduled_calls = [x for x in self._scheduled_calls
                                     if x.plugin != plugin]


class ScheduledCall(object):
    """Represents a scheduled function call"""

    def __init__(self, when=None, function=None, plugin=None,
                 args=[], kwargs={}):
        if not isinstance(when, datetime):
            when = datetime.now() + timedelta(seconds=when)
        self._time = when
        self._function = function
        self._args = args
        self._kwargs = kwargs
        self.plugin = plugin

    def due(self):
        return self._time <= datetime.now()

    def when(self):
        return self._time

    def timeout(self):
        s = (self._time - datetime.now()).total_seconds()
        return s if s > 0.0 else 0.0

    def execute(self, irc=None):
        if irc is None:
            self._function(*(self._args), **(self._kwargs))
        else:
            self._function(irc, *(self._args), **(self._kwargs))


class PeriodicScheduledCall(ScheduledCall):
    """Represents a periodic function call"""

    def __init__(self, when=None, function=None, ident=None, args=[],
                 kwargs={}, interval=None, plugin=None):
        if not isinstance(interval, timedelta):
            interval = timedelta(seconds=interval)
        if when is None:
            when = datetime.now() + interval
        super(PeriodicScheduledCall, self).__init__(when, function, plugin,
                                                    args, kwargs)
        self._interval = interval

    def execute(self, irc=None):
        """If you want the periodic call to continue, return True"""
        if irc is None:
            ret = self._function(*(self._args), **(self._kwargs))
        else:
            ret = self._function(irc, *(self._args), **(self._kwargs))
        if ret:
            self._time += self._interval
            return True
