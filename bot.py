from collections import deque
import configparser
import logging

import irc
from channels import ChannelManager
from pluginmanager import PluginManager

logger = logging.getLogger(__name__)


class Bot(irc.Protocol):
    def __init__(self, cfg_file, cfg):
        super(Bot, self).__init__()
        self.cfg = cfg
        self.cfg_file = cfg_file
        self.chmgr = ChannelManager(self)
        self.pmgr = PluginManager()

        try:
            self.plugins = [_[0] for _ in cfg.items('plugins')]
        except:
            self.plugins = []

        try:
            self.servers = deque(_[0] for _ in cfg.items('servers'))
        except:
            self.servers = deque()

        try:
            self.reconnect_delay = cfg.getint('main', 'reconnect_delay')
        except:
            self.reconnect_delay = 30

        for f in self.plugins:
            if "__init__" in f:
                continue

            self.pmgr.load_plugin(self, f)

    def connect(self):
        cfg = self.cfg

        if not self.servers:
            logger.info("Server list is empty!")
            self.reconnect()
            return

        try:
            server, port = self.servers[0].split(',')
        except:
            logger.info("Invalid server: %s" % (str(self.servers[0])))
            self.reconnect()
            return

        port = int(port)
        debug = False
        try:
            debug = cfg.getboolean('main', 'debug')
        except:
            pass

        saddr = None
        source_host = None
        source_port = 0

        try:
            source_host = cfg.get('main', 'source_host')
        except:
            pass
        try:
            source_port = cfg.getint('main', 'source_port')
        except:
            pass

        if source_host or source_port:
            saddr = (source_host, source_port)

        try:
            max_nick_tries = cfg.getint('main', 'max_nick_tries')
        except:
            pass

        try:
            nicks = cfg.get('main', 'nicks').split()
        except:
            nicks = ['crap-bot']

        try:
            username = cfg.get('main', 'username')
        except:
            username = 'i-suck'

        try:
            ircname = cfg.get('main', 'ircname')
        except:
            ircname = 'crap-bot'

        try:
            version = cfg.get('main', 'version')
        except:
            version = "crap-bot v0.0000002 by #liek"

        self.chmgr = ChannelManager(self)

        logger.info("Connecting to %s port %d" % (server, port))

        ret = irc.Protocol.connect(
            self, server, port, nicks, max_nick_tries,
            username=username, ircname=ircname,
            version=version, debug=debug, source_addr=saddr
        )

        if not ret:
            logger.info("Unable to connect!")
            self.reconnect()
            return

        try:
            self.channels = cfg.get('main', 'channels').split()
        except:
            pass

    def reconnect(self):
        if self.connected:
            return False

        if not self.registered:
            self.servers.rotate(-1)

        logger.info("Attempting reconnect in (%d)" %
                    self.reconnect_delay)
        self.schedule(when=self.reconnect_delay, function=self.connect)
        return True

    def reload_config(self):
        cfg = configparser.ConfigParser(allow_no_value=True)
        cfg.read(self.cfg_file)
        self.cfg = cfg

        try:
            new_plugins = [_[0] for _ in cfg.items('plugins')]
        except:
            new_plugins = []
        for p in new_plugins:
            if p not in self.plugins:
                self.load_plugin(p)
        for p in self.plugins:
            if p not in new_plugins:
                self.unload_plugin(p)
        self.plugins = new_plugins

        try:
            self.servers = deque(_[0] for _ in cfg.items('servers'))
        except:
            self.servers = deque()

        try:
            self.reconnect_delay = cfg.getint('main', 'reconnect_delay')
        except:
            pass

        try:
            channels = cfg.get('main', 'channels').split()
        except:
            channels = []
        if self.connected and self.registered:
            for chan in channels:
                if chan not in self.channels:
                    key = ""
                    if ':' in chan:
                        chan, key = chan.split(':', 1)
                    self.joinc(chan, key)
        self.channels = channels

        try:
            debug = cfg.getboolean('main', 'debug')
            if debug != self.debug:
                self.set_loglevel(debug)
        except:
            pass

        logger.info("Reloaded configuration")
        self.add_int(self.copy_state(), 'reload', {})

    def load_plugin(self, name):
        self.pmgr.load_plugin(self, name)

    def unload_plugin(self, name):
        plugin = self.pmgr.unload_plugin(name)
        self.scheduler_clear(plugin)

    def dispatch(self, sender, command, args):
        state = self.copy_state()
        event, kwargs = irc.Protocol.pre_dispatch(self, sender, command, args)
        kwargs['sender'] = sender
        self.pmgr.dispatch(state, event, kwargs)

        """This is going to handle things like numeric replies, nickname
        waterfalling and errors. So it's pretty important to call."""
        irc.Protocol.post_dispatch(self, event, kwargs)

    def int_dispatch(self, command, kwargs):
        self.pmgr.dispatch(self, command, kwargs)

    def dispatch_disconnect(self):
        state = self.copy_state()
        self.pmgr.dispatch(state, 'disconnect', {'sender': state.userinfo})

    """These method overrides are necessary to allow logging of outgoing
       messages."""

    def privmsg(self, who, what):
        targets = str(who).split(',')
        for t in targets:
            state = self.copy_state()
            self.add_int(state, 'privmsg_self',
                         {'target': t,
                          'message': what,
                          'sender': state.userinfo})
        irc.Protocol.privmsg(self, who, what)

    def notice(self, who, what):
        targets = str(who).split(',')
        for t in targets:
            state = self.copy_state()
            self.add_int(state, 'notice_self',
                         {'target': t,
                          'message': what,
                          'sender': state.userinfo})
        irc.Protocol.notice(self, who, what)

    def action(self, target, action):
        targets = str(target).split(',')
        for t in targets:
            state = self.copy_state()
            self.add_int(state, 'action_self',
                         {'target': t,
                          'message': action,
                          'sender': state.userinfo})
        # Overriding base classes is fun
        irc.Protocol.privmsg(self, target, "\001ACTION %s\001" % (action))

    def ctcp(self, ctcptype, target, parameter=""):
        targets = str(target).split(',')
        for t in targets:
            state = self.copy_state()
            self.add_int(state, 'ctcp_self',
                         {'target': t,
                          'command': ctcptype,
                          'data': parameter,
                          'sender': state.userinfo})
        return irc.Protocol.privmsg(self, target, "\001%s%s\001" % (
            ctcptype, parameter and (" " + parameter) or ""))

    def ctcp_reply(self, target, ctcptype, parameter):
        targets = str(target).split(',')
        for t in targets:
            state = self.copy_state()
            self.add_int(state, 'ctcpreply_self',
                         {'target': t,
                          'command': ctcptype,
                          'data': parameter,
                          'sender': state.userinfo})
        return irc.Protocol.notice(self, target, "\001%s %s\001" %
                                   (ctcptype, parameter))
