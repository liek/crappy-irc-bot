"""
Socket/Connection manager
"""

import socket
import select
import threading
from errno import EINTR
import logging
import queue

# Only defined for 3.3+, of course. Thanks
try:
    InterruptedError
except NameError:
    class InterruptedError:
        pass

logger = logging.getLogger(__name__)


class Sender(threading.Thread):
    """
    Creates a socket send thread with a queue for non-blocking I/O
    """
    def __init__(self, sock):
        assert sock
        threading.Thread.__init__(self)
        self._sock = sock
        self._queue = queue.Queue()

    def send(self, message):
        try:
            totalsent = 0
            data = message  # + "\r\n"
            msglen = len(data)
            while totalsent < msglen:
                sent = self._sock.send(data[totalsent:])
                if sent == 0:
                    self.disconnect()  # Broken socket
                    return False
                totalsent += sent
        except socket.error:
            self.disconnect()
            return False
        except AttributeError:
            self.disconnect()
            return False
        return True

    def disconnect(self):
        try:
            self._sock.shutdown(socket.SHUT_RDWR)
            #self._sock.close()
        except:
            pass

    def add(self, message):
        self._queue.put(message)

    def run(self):
        while 1:
            message = self._queue.get()
            if message is None:
                self.disconnect()
                self._queue.task_done()
                break
            else:
                status = self.send(message)
                self._queue.task_done()

                if not status:
                    break


class Receiver(threading.Thread):
    """
    Creates a thread to receive data from a socket, passing it to an
    event queue for processing.
    """

    def __init__(self, sock, recv_handler, disconnect_handler):
        assert sock
        assert recv_handler
        threading.Thread.__init__(self)
        self._sock = sock
        self._recv_handler = recv_handler
        self._disconnect_handler = disconnect_handler
        self._buffer = b''
        self.shutting_down = False

    def recv(self):
        try:
            data = self._sock.recv(2 ** 14)
        except:
            return

        return data

    def disconnect(self):
        try:
            self._sock.shutdown(socket.SHUT_RD)
            self._sock.close()
        except socket.error:
            pass
        self._disconnect_handler("Connection reset by peer")

    def run(self):
        while True:
            try:
                (rlist, wlist, exlist) = select.select(
                    [self._sock], [], [], 10)
            except InterruptedError:
                logger.debug('Reciever interrupt 1')
                continue
            except (OSError, select.error) as e:
                # Can be dropped if we remove 3.2 support
                if e.errno == EINTR:
                    logging.debug('Reciever interrupt 2')
                    continue
                else:
                    raise

            if self.shutting_down:
                logger.debug("Shutdown triggered")
                self.disconnect()
                break

            if self._sock not in rlist:
                continue

            data = self.recv()
            if not data:
                logger.debug('Reciever calling disconnect()')
                self.disconnect()
                break

            self._recv_handler(data)


class Connection(object):
    """
    Creates a connection object responsible for managing connecting to a
    server, launching sending and receiving threads.
    """
    def __init__(self, recv_handler, disconnect_handler):
        self._sock = None
        self._sender = None
        self._receiver = None
        self._recv_handler = recv_handler
        self._disconnect_handler = disconnect_handler

    def connect(self, server, port, source_address):
        self.server = server
        self.port = port
        self.source_address = source_address

        if self._sender and self._sender.is_alive():
            return False
        if self._receiver and self._receiver.is_alive():
            return False

        try:
            self._sock = socket.create_connection(
                (self.server, self.port),
                socket.getdefaulttimeout(), self.source_address)
        except socket.error:
            return False

        self._sender = Sender(self._sock)
        self._receiver = Receiver(self._sock, self._recv_handler,
                                  self._disconnect_handler)
        self._sender.start()
        self._receiver.start()
        return True

    def send(self, message):
        if self._sender and self._sender.is_alive():
            self._sender.add(message)

    def connected(self):
        if self._sender and self._receiver:
            if self._sender.is_alive and self._receiver.is_alive():
                return True
        else:
            return False

    def disconnect(self):
        if self._sender:
            logger.debug("Shutting down sender...")
            self._sender.add(None)
            self._sender.join()
            logger.debug("Sender shutdown")
        if self._receiver:
            logger.debug("Waiting for reciever to shut down...")
            self._receiver.shutting_down = True
            self._receiver.join()
            logger.debug("Reciever shutdown")
        self._sender = None
        self._receiver = None
        self._sock = None
        return
