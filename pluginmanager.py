from collections import defaultdict
import threading
import sys
import queue
import logging

import binds

reserved = ['bind', 'scheduler']

logger = logging.getLogger(__name__)


class Plugin(threading.Thread):
    def __init__(self, pmgr):
        threading.Thread.__init__(self)
        self._queue = queue.Queue()
        self.loaded = False
        self._events = dict()
        self._pmgr = pmgr

    def load(self, module, bot):
        if self.loaded:
            return False
        self.logger = logging.getLogger(module)
        self.module_name = module
        pname = module

        try:
            plugin = __import__(pname, fromlist=[''])
        except ImportError as ie:
            logger.info("Could not import: %s (%s)" % (pname, ie))
            logger.exception("Error importing plugin")
            plugin = None
            return False

        if plugin:
            try:
                logger.info("Loading %s" % pname)
                plugin.init(self, bot)
            except:
                logger.info("Error loading %s" % pname)
                logger.exception("Error loading plugin")
                self._unload_plugin()
                return False
        self.loaded = True
        return True

    def subscribe(self, event, func):
        if event in reserved:
            raise Exception("Attempt to bind reserved event: %s" % event)
        self._events[event] = func

    def events(self):
        return self._events.keys()

    def schedule(self, bot, *args, **kwargs):
        kwargs['plugin'] = self
        bot.schedule(*args, **kwargs)

    def schedule_periodic(self, bot, *args, **kwargs):
        kwargs['plugin'] = self
        bot.schedule_periodic(*args, **kwargs)

    def bind_public(self, trigger, func, split=True, maxsplits=-1):
        self._pmgr.binds.add('pubmsg', self, trigger, func, split, maxsplits)

    def bind_private(self, trigger, func, split=True, maxsplits=-1):
        self._pmgr.binds.add('privmsg', self, trigger, func, split, maxsplits)

    def add(self, event=None, bot=None, kwargs={}):
        if not event:
            self._queue.put(None)
        self._queue.put((event, bot, kwargs))

    def run(self):
        def _handle_bind(bot, func, **kwargs):
            try:
                func(bot, **kwargs)
            except:
                logger.info("Error in plugin %s" % self.module_name)
                logger.exception("Plugin error")

        def _handle_scheduler(bot, sch, **kwargs):
            try:
                if sch.execute(bot):
                    bot._schedule(sch)
            except:
                logger.info("Error in scheduled call: %s" % self.module_name)
                logger.exception("Scheduled call error")

        self._events['bind'] = _handle_bind
        self._events['scheduler'] = _handle_scheduler

        while 1:
            msg = self._queue.get()
            if msg is None:
                self._queue.task_done()
                break
            event, bot, kwargs = msg
            try:
                self._events[event](bot, **kwargs)
            except:
                logger.info("Error in plugin %s" % self.module_name)
                logger.exception("Plugin error")
            self._queue.task_done()

        self._unload_plugin()

    def _unload_plugin(self):
        pname = self.module_name
        f = pname
        if "." in f:
            f = f[f.rfind('.') + 1:]

        try:
            plugin = sys.modules[pname]
        except KeyError:
            return

        try:
            plugin.unload()
        except AttributeError:
            pass
        except:
            logger.info("Error while unloading %s" % pname)
            logger.exception("Error unloading plugin")

        logger.info("Unloaded %s" % pname)

        del sys.modules[pname]
        del sys.modules['plugin'].__dict__[f]
        del plugin


class PluginManager(object):
    def __init__(self):
        self._plugins = dict()
        self._unloading = dict()
        self._events = defaultdict(list)
        self.mutex = threading.RLock()
        self.binds = binds.Binds()

    def load_plugin(self, bot, name):
        with self.mutex:
            self.check_plugins()
            pname = self.get_plugin_name(name)
            if pname in self._plugins:
                logger.info("Attempted to load %s, which is already loaded" %
                            pname)
                return False
            if pname in self._unloading:
                logger.info("Attempted to load %s, which is unloading" % pname)
                return False

            plugin = Plugin(self)
            if plugin.load(pname, bot):
                for event in plugin.events():
                    self._events[event].append(plugin)
                self._plugins[pname] = plugin
                plugin.start()
            else:
                self.binds.clear(plugin)
                return False

    def unload_plugin(self, name):
        with self.mutex:
            self.check_plugins()
            pname = self.get_plugin_name(name)

            if pname not in self._plugins:
                logger.info("Attempted to unload %s, which wasn't loaded" %
                            pname)
                return False

            plugin = self._plugins[pname]

            self._unloading[pname] = plugin
            del self._plugins[pname]

            for event in self._events:
                if plugin in self._events[event]:
                    self._events[event].remove(plugin)

            self.binds.clear(plugin)

            plugin.add(None)
            return plugin

    def get_plugin_name(self, name):
        if "." in name:
            pname = name
        else:
            pname = "plugin." + name

        return pname

    def check_plugins(self):
        with self.mutex:
            for pname in list(self._unloading):
                if not self._unloading[pname].is_alive():
                    plugin = self._unloading[pname]
                    del self._unloading[pname]
                    del plugin

    def shutdown_plugins(self):
        with self.mutex:
            self.check_plugins()
            logger.info("Unloading plugins")
            for pname in list(self._plugins.keys()):
                self.unload_plugin(pname)

            for pname in self._unloading:
                self._unloading[pname].join()
            self.check_plugins()
            logger.info("Plugin unloading complete")

            return True

    def dispatch(self, bot, event, kwargs):
        with self.mutex:
            if event in self._events:
                for p in self._events[event]:
                    p.add(event, bot, kwargs)
            if event in self.binds.events:
                self.binds.call(event, bot, kwargs)
