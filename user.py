import copy
import re


class User(object):
    m_source = re.compile(r'([^!]*)!?([^@]*)@?(.*)')

    @classmethod
    def from_hostmask(cls, source_str):
        match = User.m_source.match(source_str or '')
        return cls(*match.groups())

    def __init__(self, nick, username=None, host=None):
        self.nick, self.username, self.host = nick, username, host

    def get_usermask(self):
        user = self.username

        if len(user) > 1 and user[0] == "~":
            user = user[1:]

        user = '*' + user

        if ':' in self.host:
            return "%s@%s" % (user, self.host)

        ip = re.match(r'^(\d{1,3})\.(\d{1,3})\.\d{1,3}\.\d{1,3}$', self.host)

        if ip:
            return "%s@%s.%s.*.*" % (user, ip.group(1), ip.group(2))

        domain = re.match(r'^.*\.(\w+)\.(\w+)$', self.host)

        if domain:
            return "%s@*.%s.%s" % (user, domain.group(1), domain.group(2))

        return "%s@%s" % (user, self.host)

    def __repr__(self):
        return "<User: %s %s %s>" % (self.nick, self.username, self.host)

    def __str__(self):
        if self.username and self.host:
            return "%s!%s@%s" % (self.nick, self.username, self.host)
        else:
            return self.nick


class ChannelUser(User):
    def __init__(self, nick='', username='unknown', host='', modes=set()):
        super(ChannelUser, self).__init__(nick, username, host)
        self._modes = modes

    def add_mode(self, mode):
        if mode:
            self._modes.add(mode)

    def remove_mode(self, mode):
        if mode:
            self._modes.discard(mode)

    def isop(self):
        return "o" in self._modes

    def isvoice(self):
        return "v" in self._modes

    def ishalf(self):
        return "h" in self._modes

    def modes(self):
        return ''.join(self._modes)

    def copy(self):
        user = copy.copy(self)
        user._modes = copy.deepcopy(self._modes)
        return user
