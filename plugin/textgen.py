"""This module requires lxml and nltk!"""
from lxml.cssselect import CSSSelector
import lxml.html
import nltk  # nltk.model.* was removed in nltk-3
import re
import warnings
import base64
from io import BytesIO
import os
import gzip
import fnmatch
import random

import urllib.request as UL

content = []
plugin = None

plog_url = 'http://www.liek.net/plogs/'
plog_user = 'user'
plog_password = 'password'
plog_aliases = []

warnings.simplefilter('ignore')

m_line = re.compile(r'^<.*?> (.*)')


def init(p, bot):
    global plugin, plog_user, plog_password, plog_aliases
    plugin = p
    plugin.subscribe('pubmsg', pubmsg)

    try:
        plog_user = bot.cfg.get('textgen', 'plog_user')
    except:
        pass
    try:
        plog_password = bot.cfg.get('textgen', 'plog_password')
    except:
        pass
    try:
        fh = open('tgaliases.txt', 'r', encoding='utf-8')
        for line in fh:
            line = line.rstrip()
            try:
                plog_aliases.append(re.match(r'(.*?) = (.*)', line).groups())
            except:
                pass
        fh.close()
    except:
        pass


def pubmsg(irc, sender, channel, message):
    less = False

    lookup = re.search(r'say something like (\w+)', message)
    if not lookup:
        lookup = re.search(r'say something less random like (\w+)', message)
        less = True
    if lookup:
        name = irc.irclower(lookup.groups()[0])
        if name == 'me':
            name = irc.irclower(sender.nick)
        if name == 'us':
            name = '-all'
        else:
            for i in plog_aliases:
                if fnmatch.fnmatch(name, irc.irclower(i[0])):
                    name = i[1]
                    break
        text = generate_from_log(plog_url + name + '.log', name + '.log',
                                 plog_user, plog_password, less)
        if text is None:
            irc.privmsg(channel, '%s: Unable to fetch log' % (sender.nick))
            return
        stok = nltk.tokenize.punkt.PunktSentenceTokenizer()
        sentences = stok.tokenize(text)
        sentence = max(sentences, key=len)
        irc.privmsg(channel, sentence)
    elif 'say something' in message.lower():
        text = generate()

        stok = nltk.tokenize.punkt.PunktSentenceTokenizer()
        sentences = stok.tokenize(text)
        sentence = max(sentences, key=len)
        irc.privmsg(channel, sentence)


def fetch_page(page_url, user=None, password=None):
    try:
        req = UL.Request(page_url)
        if user and password:
            auth_str = base64.encodestring(
                ('%s:%s' %
                 (user, password)).encode('utf-8')).replace(b'\n', b'')
            req.add_header("Authorization", "Basic %s" %
                           auth_str.decode('utf-8'))
        req.add_header('Accept-encoding', 'gzip')
        url = UL.urlopen(req)
        if url.headers.get('Content-Encoding', "") == 'gzip':
            buf = BytesIO(url.read())
            temp = gzip.GzipFile(fileobj=buf)
            data = temp.read()
        else:
            data = url.read()
        return data
    except:
        return None


def parse_html(html):
    root = lxml.html.fromstring(html.decode('utf-8'))
    sel = CSSSelector('p.qt')
    quotes = sel(root)

    tokenizer = nltk.tokenize.regexp.WordPunctTokenizer()

    for quote in quotes:
        text = quote.text_content()
        for line in text.split('\n'):
            pieces = line.split(' ')
            sentence = ' '.join(pieces[1:])
            if sentence[-1:] not in '.,!?':
                sentence = sentence + '.'
            words = tokenizer.tokenize(sentence)
            for word in words:
                content.append(word)


def parse_log(log, less_random=False):
    log_content = []
    tokenizer = nltk.tokenize.regexp.WordPunctTokenizer()
    lines = []
    for text in log.split('\n'):
        lm = m_line.match(text)
        if lm:
            line = lm.groups()[0]
            if len(line) > 20:
                lines.append(line)
    if less_random:
        idx = 0
        if len(lines) > 500:
            idx = random.randint(0, len(lines) - 500)
        lines = lines[idx:idx + 500]
    else:
        if len(lines) > 5000:
            lines = random.sample(lines, 5000)
    for line in lines:
        pieces = line.split(' ')
        sentence = ' '.join(pieces[1:])
        if sentence[-1:] not in '.,!?':
            sentence = sentence + '.'
        words = tokenizer.tokenize(sentence)
        for word in words:
            log_content.append(word)
    return log_content


def generate_from_log(from_url, logname, user=None, password=None,
                      less_random=False):
    if os.path.exists('plogs/' + logname):
        try:
            fh = open('plogs/' + logname, 'r', encoding='utf-8')
            log = fh.read()
            fh.close()
        except IOError:
            return None
    else:
        log = fetch_page(from_url, user, password)
        if not log:
            return None
        log = log.decode('utf-8', 'ignore')
        try:
            fh = open('plogs/' + logname, 'w+', encoding='utf-8')
            fh.write(log)
            fh.close()
        except:
            pass
    log_content = parse_log(log, less_random)

    model = nltk.model.ngram.NgramModel(3, log_content)
    starting = model.generate(100)[-2:]
    words = model.generate(120, starting)
    out = fix_punct(words)
    return out


def generate(from_url='http://bash.org/?random1'):
    for i in range(0, 4):
        html = fetch_page(from_url)
        if not html:
            return None
        parse_html(html)

    model = nltk.model.ngram.NgramModel(3, content)
    starting = model.generate(100)[-2:]
    #out = ' '.join(model.generate(100, starting))
    #out = re.sub(' ?(\\.|,|!|\\?|:|;|\\(|\\)|\'|") ?', '\\1', out)
    #out = re.sub('(\\.|,|!|\\?|:|;)([a-zA-Z0-9])', '\\1 \\2', out)
    words = model.generate(120, starting)
    out = fix_punct(words)

    return out


def fix_punct(content):
    output = ''
    punct = """,.?!;":'()`"""
    last = ''
    for w in content:
        if last and last not in punct:
            if w not in ',.?!\'`)' or w == '(':
                output += ' '
        if last and last in punct and w in punct:
            pass
        elif last and last in punct and w not in punct:
            if last in '.?!;,:)':
                output += ' '
                if last in '.?!':
                    w = w.capitalize()
        if not output:
            w = w.capitalize()
        output += w
        last = w

    output = output.replace(" - ", "-") + '.'

    w = output.split()
    if "re" == w[0].lower():
        w[0] = random.choice(["They're", "We're", "You're"])
    elif "ll" == w[0].lower():
        w[0] = random.choice(["I'll", "We'll", "They'll", "You'll"])
    elif "m" == w[0].lower():
        w[0] = "I'm"
    elif "t" == w[0].lower():
        w[0] = random.choice(["Can't", "Didn't", "Don't", "Isn't"])
    elif "ve" == w[0].lower():
        w[0] = random.choice(["I've", "We've", "You've", "They've"])
    elif "s" == w[0].lower():
        w[0] = random.choice(["It's", "That's"])
    elif w[0][0] in ",.!?'":
        x = w.pop(0)
        while x[0] in ",.!?'":
            x = w.pop(0)

    w[0] = w[0].capitalize()
    return " ".join(w)
