from __future__ import unicode_literals
import threading
import json

from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
from urllib.parse import unquote_plus

# Git push announcer plugin
# Code lifted from sloat's webadmin plugin :)

irc = None
plugin = None
auth_key = "secure"
announce_channel = "#liek"
httpd = None


def init(p, bot):
    global irc, plugin, auth_key, announce_channel
    irc = bot
    plugin = p

    wa = GitWatcherThread(irc=bot)
    wa.start()

    try:
        auth_key = irc.cfg.get('gitwatcher', 'auth_key')
    except:
        pass

    try:
        announce_channel = irc.cfg.get('gitwatcher', 'channel')
    except:
        pass


def unload():
    global httpd
    if httpd:
        httpd.shutdown()


class GitWatcherThread(threading.Thread):
    def __init__(self, group=None, target=None, name=None, *args, **kwargs):
        threading.Thread.__init__(self, group, target, name, args, kwargs)
        self.irc = kwargs['irc']
        self.daemon = True

    def run(self):
        global httpd
        addr_info = ('', 8042)
        httpd = GitWatcherWebServer(addr_info, GitWatcherHandler)
        #while True:
            #if not self.irc.connected:
            #    break
            #httpd.handle_request()
        httpd.serve_forever()


class GitWatcherWebServer(ThreadingMixIn, HTTPServer):
    pass


class GitWatcherHandler(BaseHTTPRequestHandler):
    def __init__(self, request, client_address, server):
        BaseHTTPRequestHandler.__init__(self, request, client_address,
                                        server)

    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()

        self.wfile.write("Rabbit season!\r\n\r\n".encode('utf-8'))

    def do_POST(self):
        global irc, auth_key, announce_channel
        self.cmd = self.path
        try:
            data_len = int(self.headers['Content-Length'])
        except:
            data_len = int(self.headers.getheader('Content-Length'))
        self.post_data = self.rfile.read(data_len).decode('utf-8')
        self.post_data = unquote_plus(self.post_data)
        if self.post_data.startswith('payload='):
            self.post_data = self.post_data[8:]

        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()

        # BitBucket can't send a basic auth header apparently, wow.

        #if self.headers.getheader("Authorization") != auth_key:
        #    self.wfile.write("go away\r\n\r\n")
        #    h = str(self.headers).split("\n")
        #    for i in h:
        #       irc.privmsg("#liek", i)
        #    return

        if self.cmd != ("/" + auth_key):
            self.wfile.write("What's up doc?\r\n\r\n".encode('utf-8'))
            return

        try:
            data = json.loads(self.post_data)
        except:
            return

        try:
            if 'issue_id' in data:
                irc.privmsg(announce_channel,
                            "\002CIB Issue\002 #%s created: %s" %
                            (data['issue_id'], data['issue_title']))
                irc.privmsg(announce_channel, data['issue_url'])
                self.wfile.write("Beep Beep\r\n".encode('utf-8'))
                return

            irc.privmsg(announce_channel,
                        "\002Git Thrust\002 \037%s\037 %d changeset(s)" %
                        (data['repository']['name'],
                            len(data['commits'])))

            for c in data['commits']:
                irc.privmsg(announce_channel,
                            "\002Branch\002: %s \002Author\002: %s \002Commit\002: %s" %
                            (c['branch'], c['author'], c['node']))
                msg = c['message'].split("\n")
                for line in msg:
                    irc.privmsg(announce_channel, line)
        except:
            pass

        json.dump(data, open('gitwatch.json', 'w+'), sort_keys=True,
                  indent=4, separators=(',', ': '))

        self.wfile.write("Duck season!\r\n\r\n".encode('utf-8'))
