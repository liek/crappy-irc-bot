plugin = None

my_nick = ""
my_timer = None


def init(p, bot):
    global plugin, my_nick
    plugin = p
    irc = bot
    plugin.subscribe('quit', handle_quit)
    plugin.subscribe('rpl_welcome', handle_001)
    plugin.subscribe('nick', handle_nick)

    try:
        nicks = irc.cfg.get('main', 'nicks').split()
        my_nick = nicks[0]
    except:
        pass

    plugin.logger.info('nickregain.py by Matt@#liek loaded.')


def unload():
    pass


def handle_quit(irc, sender, message):
    if not my_nick:
        return

    if irc.irccmp(sender.nick, my_nick) == 0:
        irc.nick(my_nick)


def handle_001(irc, sender, args):
    if my_nick and args[0] != my_nick:
        plugin.schedule(irc, when=30.0, function=regain_nick)


def handle_nick(irc, sender, newnick):
    if not my_nick:
        return

    if sender.nick != irc.nickname and irc.irccmp(sender.nick, my_nick) == 0:
        irc.nick(my_nick)


def regain_nick(irc):
    if my_nick and irc.nickname != my_nick:
        irc.nick(my_nick)
        plugin.schedule(irc, when=30.0, function=regain_nick)
