"""
weather.py -- Weather data plugin for CIB.
 Data provided by weather.gov

"""

import urllib.request
from urllib.parse import urlencode
from datetime import datetime, timedelta

from xml.etree import ElementTree

plugin = None

def init(p, bot):
    global plugin
    plugin = p

    plugin.bind_public('!w', handle_w)


def handle_w(irc, sender, channel, args):
    if len(args) < 2:
        irc.privmsg(channel,
                'Need a zipcode, jerk')
        return
    params = [
            'temp',
            'maxt',
            'mint',
            'appt',
            'pop12',
            ]

    now = datetime.utcnow()
    end = now + timedelta(minutes=1)
    params = dict(zip(params, params))
    params['zipCodeList'] = args[1]
    params['product'] = 'time-series'
    params['begin'] = now.strftime('%Y-%m-%dT%H:%M:%S')
    params['end'] = end.strftime('%Y-%m-%dT%H:%M:%S')

    query = urlencode(params)

    url = 'http://graphical.weather.gov/xml/sample_products/'\
            'browser_interface/ndfdXMLclient.php?{}'.format(query)

    req = urllib.request.urlopen(url)

    xmlsrc = req.read()

    root = ElementTree.fromstring(xmlsrc)

    min_temp = root.find(".//parameters/temperature[@type='minimum']/value")
    max_temp = root.find(".//parameters/temperature[@type='maximum']/value")

    if min_temp is not None:
        ext_temp = min_temp.text
        minmax_label = 'Low'

    elif max_temp is not None:
        ext_temp = max_temp.text
        minmax_label = 'High'

    else:
        ext_temp = '???'
        minmax_label = 'High/Low'

    current = root.find('.//parameters/temperature[@type="hourly"]/value')
    temp = current.text if current is not None else 'whoops'

    apparent = root.find('.//parameters/temperature[@type="apparent"]/value')
    appr = apparent.text if apparent is not None else 'damn it'

    pop = root.find('.//parameters/probability-of-precipitation/value')
    pct = pop.text if pop is not None else 'oh well'

    message = "Here's your stupid weather: "\
            "Current: {}, Feels like: {}, {}: {}, "\
            "Chance of precipitation: {}%".format(
                    temp, appr, minmax_label, 
                    ext_temp, pct)

    irc.privmsg(channel, message)

    #print(message)

#if __name__ == '__main__':
    #handle_w(None, None, '', ['test', '19720'])
