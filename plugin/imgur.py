import re
import time
import requests

imgur_client_id = ""

plugin = None
m_gurl = re.compile(
    r'https?://(?:www\.|m\.|i\.)?imgur\.com/(gallery/|a/)?([^\s,\./]{5,})')
# m_iurl = re.compile(r'i\.imgur\.com/([^\s\.]{5,})')
m_bmsg = re.compile(r'\[nsfw: \w+\]')


def init(p, bot):
    global plugin, imgur_client_id

    irc = bot
    plugin = p
    plugin.subscribe('pubmsg', handle_pubmsg)
    try:
        imgur_client_id = irc.cfg['imgur']['client_id']
    except KeyError:
        plugin.logger.info('Imgur client-id not found in config.')
    plugin.logger.info("imgur.py by Matt@#liek loaded.")


def handle_pubmsg(irc, sender, channel, message):
    if 'imgur' not in message:
        return

    m = m_gurl.search(message)
    if m:
        if not m_bmsg.search(message):
            show_image(irc, sender, channel, *m.groups()[::-1])


def get_image(img_id, g=""):
    headers = {'Authorization': 'Client-ID ' + imgur_client_id}

    try:
        # Imgur API endpoints are shit. Tries to find as much info as
        # possible.
        if g:
            res = requests.get('https://api.imgur.com/3/gallery/' + img_id,
                               headers=headers, timeout=5)
        else:
            res = requests.get(
                'https://api.imgur.com/3/gallery/image/' + img_id,
                headers=headers, timeout=5)
        if not res:
            res = requests.get('https://api.imgur.com/3/image/' + img_id,
                               headers=headers, timeout=5)
    except requests.RequestException:
        plugin.logger.exception('Imgur lookup error')
        raise ImgurError('Unable to connect to imgur')

    try:
        data = res.json()
    except ValueError:
        plugin.logger.exception('Imgur lookup error')
        raise ImgurError('Unable to read imgur data')
    return data


def show_image(irc, sender, target, img_id, g=""):
    try:
        res = get_image(img_id, g)
        data = res['data']
    except ImgurError as e:
        irc.privmsg(target, "%s: %s" % (sender.nick, str(e)))
        return
    title = '(no title)'
    time_added = ""
    width = 0
    height = 0
    views = 0
    dim = ""
    nsfw = "unknown"
    imgs = 0

    try:
        title = data['title'] if data['title'] else '(no title)'
    except KeyError:
        pass
    try:
        time_added = time.strftime('%m/%d/%y',
                                   time.localtime(data['datetime']))
    except (KeyError, ValueError):
        pass
    try:
        width = int(data['width'])
    except (KeyError, ValueError):
        pass
    try:
        height = int(data['height'])
    except (KeyError, ValueError):
        pass
    if width and height:
        dim = '%dx%dpx' % (width, height)
    try:
        views = int(data['views'])
    except (KeyError, ValueError):
        pass
    try:
        nsfw = str(data['nsfw'])
    except (KeyError, ValueError):
        pass
    try:
        if data['is_album']:
            imgs = len(data['images'])
    except KeyError:
        pass

    def pluralize(word, n):
        return "{:,} {}{}".format(n, word, 's' if n != 1 else '')

    msg = '%s: \037%s\037 (%s) %shas been viewed %s. [nsfw: %s]' % (
        sender.nick, title,
        dim if dim else pluralize('image', imgs) if imgs else '',
        'added %s ' % time_added if time_added else '',
        pluralize('time', views), nsfw)
    irc.privmsg(target, msg)


class ImgurError(Exception):
    pass
