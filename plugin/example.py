# Example plugin script
# needs to define init() ... otherwise it's dumped after load
import sys
from datetime import timedelta

plugin = None


def init(p, bot):
    global plugin
    plugin = p
    plugin.subscribe('pubmsg', pubmsg)
    plugin.subscribe('notice', notice)
    plugin.subscribe('join', join)
    plugin.schedule_periodic(bot, function=schedule_test,
                             interval=timedelta(seconds=15))
    plugin.bind_public('!test', handle_test)


def join(irc, sender, channel):
    ops = ["kif.angrycoder.org", "c-76-25-4-66.hsd1.co.comcast.net"]
    if sender.host in ops:
        irc.mode(channel, "+o", sender.nick)
    pass


def notice(irc, sender, message):
    if message == "get lost":
        irc.quit("quit command recieved from %s" % sender.nick)
        pass


def pubmsg(irc, sender, channel, message):
    msg = message.split()

    if msg:
        if msg[0] == "authtest":
            if 'plugin.control' in sys.modules:
                if sys.modules['plugin.control'].is_authenticated(sender):
                    irc.privmsg(channel, "Auth test - Success")
                else:
                    irc.privmsg(channel, "Auth test - Failure")
            else:
                irc.privmsg(channel, "Couldn't find control plugin")
        if msg[0] == "!suck":
            irc.privmsg(channel, "Man... sloat sucks.")
        if msg[0] == "!assrape":
            if irc.chmgr.get(channel).isop(sender.nick):
                try:
                    name = msg[1]
                except:
                    name = ""

                if irc.nickname == name:
                    irc.kick(channel,
                             sender.nick,
                             "Assraped %s by request" % (sender.nick))
                else:
                    irc.kick(channel, name, "Assraped")
        if msg[0] == "debug":
            plugin.logger.info(irc.chmgr.channels)
        if msg[0] == "ops":
            chan = irc.chmgr.get(channel)
            irc.privmsg(channel, "Ops: %s" % " ".join(chan.ops()))
        if msg[0] == "voices":
            chan = irc.chmgr.get(channel)
            irc.privmsg(channel, "Voices: %s" % " ".join(chan.voices()))
        if msg[0] == "test":
            for a in range(5):
                irc.privmsg(channel, "Test %d" % a)
        if msg[0] == "test2":
            irc.privmsg("#liek,#test2", "Testing.")
        if sender.nick == 'kaboofa':
            if msg[0] == "load":
                irc.load_plugin(msg[1])
            elif msg[0] == "unload":
                irc.unload_plugin(msg[1])
        if msg[0] == 'topic':
            ch = irc.chmgr.get(channel)
            plugin.logger.info(ch)
            irc.privmsg(channel, "Topic is: %s" % (ch.topic))
            irc.privmsg(channel,
                        "Topic set by %s on %s" %
                        (ch.topic_setter, ch.topic_time))
        if msg[0] == 'bans':
            ch = irc.chmgr.get(channel)
            for b in ch.bans:
                irc.privmsg(channel, "%s - %s - %s" %
                            (b, ch.bans[b]['setter'], ch.bans[b]['when']))
    if message == "get lost":
        if irc.chmgr.get(channel).isop(sender.nick):
            irc.terminate("you're the boss")
            pass


def schedule_test(irc):
    irc.privmsg("#liek", "Testing")
    return True


def handle_test(irc, sender, channel, data):
    irc.privmsg(channel.channel, '%s sucks.' % (data[0] if data else 'sloat'))
