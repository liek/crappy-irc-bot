from copy import deepcopy
import json

import urllib.request

plugin = None
irc = None
myLocations = {}


def init(p, bot):
    global irc, plugin
    irc = bot
    plugin = p
    plugin.bind_public('!snow', snow)


def load_locations(whichState):
    global myLocations

    url = 'http://opensnow.com/api/public/1.0/locations?apikey=zasada' \
          '&state=%s&type=json' % whichState

    req = urllib.request.Request(url, headers={'User-Agent' : "Magic Browser"})

    jsonRaw = urllib.request.urlopen(req).read()
    tempObj = json.loads(jsonRaw.decode())
    myLocations[whichState] = tempObj['results']


def find_location(whichState, whichArea):
    global myLocations

    retreivedLocation = myLocations.get(whichState)

    for location in retreivedLocation:
        if retreivedLocation[location]['name'].lower() == whichArea.lower():
            return retreivedLocation[location]


def get_forecast(locationID):
    url = 'http://opensnow.com/api/public/1.0/locations/data?apikey=zasada' \
          '&lids=%s&type=json' % locationID

    req = urllib.request.Request(url, headers={'User-Agent' : "Magic Browser"})

    jsonRaw = urllib.request.urlopen(req).read()
    myJson = json.loads(jsonRaw.decode())

    locationKey = 'location%s' % locationID

    myJson = myJson['results']

    forecast = []

    del myJson[locationKey]['forecast']['updated_at']

    weekTotal = [0, 0]
    for forecastChunkKey in myJson[locationKey]['forecast']:
        forecastChunk = myJson[locationKey]['forecast'][forecastChunkKey]
        dayTotal = forecastChunk['day']['snow']
        nightTotal = forecastChunk['night']['snow']
        if "-" in forecastChunk['day']['snow']:
            dayTotal = forecastChunk['day']['snow'].split('-')
        if "-" in forecastChunk['night']['snow']:
            nightTotal = forecastChunk['night']['snow'].split('-')

        if isinstance(dayTotal, list) and isinstance(nightTotal, list):
            weekTotal[0] += (int(dayTotal[0]) + int(nightTotal[0]))
            weekTotal[1] += (int(dayTotal[1]) + int(nightTotal[1]))
            tmpForecast = ("%s: %d-%d\"" %
                           (forecastChunk['dow'],
                            int(dayTotal[0]) + int(nightTotal[0]),
                            int(dayTotal[1]) + int(nightTotal[1])))
        elif isinstance(dayTotal, list):
            weekTotal[0] += (int(dayTotal[0]) + int(nightTotal))
            weekTotal[1] += (int(dayTotal[1]) + int(nightTotal))
            tmpForecast = ("%s: %d-%d\"" %
                           (forecastChunk['dow'],
                            int(dayTotal[0]) + int(nightTotal),
                            int(dayTotal[1]) + int(nightTotal)))
        elif isinstance(nightTotal, list):
            weekTotal[0] += (int(dayTotal) + int(nightTotal[0]))
            weekTotal[1] += (int(dayTotal) + int(nightTotal[1]))
            tmpForecast = ("%s: %d-%d\"" %
                           (forecastChunk['dow'],
                            int(dayTotal) + int(nightTotal[0]),
                            int(dayTotal) + int(nightTotal[1])))
        else:
            weekTotal[0] += (int(dayTotal) + int(nightTotal))
            weekTotal[1] += (int(dayTotal) + int(nightTotal))
            tmpForecast = ("%s: %d\"" %
                           (forecastChunk['dow'],
                            int(dayTotal) + int(nightTotal)))
        forecast.append(tmpForecast)

    if weekTotal[0] == weekTotal[1]:
        forecast.append(("Total: %d\"" % weekTotal[0]))
    else:
        forecast.append(("Total: %d-%d\"" % (weekTotal[0], weekTotal[1])))

    return '; '.join(forecast)


def snow(irc, sender, channel, args):
    global myLocations

    if len(args) < 2 or args[1] == 'help':
        irc.notice(sender.nick,
                   "Feature is incomplete, haha sucks for you.")
        return
    elif args[1] == 'list':
        if len(args) < 3:
            myState = 'CO'
        else:
            myState = args[2]

        if myLocations.get(myState) is None:
            load_locations(myState)

        myList = []
        for location in myLocations.get(myState):
            myList.append(location['name'])

        irc.notice(sender.nick, ", ".join(myList))

    else:
        if(len(args) < 3):
            irc.privmsg(
                channel,
                "Wrong format, must be: !snow <location>, <state>")
            return

        msg_copy = deepcopy(args)
        del msg_copy[0]
        msg_copy = " ".join(msg_copy)
        msg_copy = msg_copy.split(',')

        skiArea = msg_copy[0].strip()
        skiState = msg_copy[1].strip()

        if skiState is not None and myLocations.get(skiState) is None:
            load_locations(skiState)

        myLocation = find_location(skiState, skiArea)
        forecast = get_forecast(myLocation['id'])

        irc.privmsg(channel, "%s: %s" % (myLocation['name'], forecast))
