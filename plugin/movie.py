import json

import urllib.request as UL
from urllib.parse import quote_plus


plugin = None
rt_apikey = ""

rt_data = None
rt_idx = 0
rt_page = 1
rt_query = ""


def init(p, bot):
    global plugin, rt_apikey
    plugin = p
    irc = bot
    plugin.bind_public('!ms', lookup_movie, False)
    try:
        rt_apikey = irc.cfg.get('movie', 'rt_apikey')
    except:
        pass
    plugin.logger.info("movie.py by Matt@#liek loaded.")


def lookup_movie(irc, sender, channel, args):
    global rt_data, rt_idx, rt_query, rt_page
    lookup = False
    if not args[1] or args[1].isspace():
        irc.notice(sender.nick, 'Syntax: !ms (movie title)')
        return
    q = args[1].rstrip()
    if q == '-next' or (rt_data and q == rt_query):
        if rt_data is None:
            irc.privmsg(channel,
                        "%s: You need to look up a movie first." %
                        (sender.nick))
            return
        rt_idx += 1
        if 'total' in rt_data and rt_idx >= rt_data['total']:
            irc.privmsg(channel, '%s: No more results.' % (sender.nick))
            rt_data = None
            return
        if rt_idx % 30 == 0:
            rt_page += 1
            lookup = True
    else:
        lookup = True
        rt_query = q
        rt_page = 1
        rt_idx = 0
    if lookup:
        try:
            url = 'http://api.rottentomatoes.com/api/public/v1.0/' \
                'movies.json?q=' + quote_plus(rt_query) + \
                '&page_limit=30&page=' + str(rt_page) + '&apikey=' + \
                quote_plus(rt_apikey)
            res = UL.urlopen(url)
            data = res.read().decode('utf-8')
            rt_data = json.loads(data)
        except:
            irc.privmsg(channel,
                        "%s: Unable to lookup that movie." % (sender.nick))
            rt_data = None
            return
    try:
        if rt_data['total'] < 1:
            irc.privmsg(channel,
                        "%s: I couldn't find that movie." % (sender.nick))
            rt_data = None
            return
        rt_movie = rt_data['movies'][rt_idx % 10]

        title = "(unknown)"
        critics_score = -1
        audience_score = -1
        year = ""
        #runtime = ""
        cast = "(unknown)"

        try:
            title = rt_movie['title']
        except KeyError:
            pass
        try:
            year = rt_movie['year']
            year_text = ""
            if type(year) == int:
                year_text = " (%d)" % (year)
        except KeyError:
            pass
        #try:
        #    runtime = rt_movie['runtime']
        #except KeyError:
        #    pass
        try:
            critics_score = rt_movie['ratings']['critics_score']
        except KeyError:
            pass
        try:
            audience_score = rt_movie['ratings']['audience_score']
        except KeyError:
            pass
        try:
            cast_list = [i['name'] for i in rt_movie['abridged_cast']]
            if len(cast_list) > 1:
                cast = ', '.join(cast_list[:-1]) + ' and ' + cast_list[-1]
            elif len(cast_list) == 1:
                cast = cast_list[0]
        except KeyError:
            pass

        if critics_score == -1 and audience_score == -1:
            rating_text = "hasn't been rated"
        elif critics_score == -1:
            rating_text = "hasn't been rated by critics, but has an " \
                "audience score of \002%d%%\002" % (audience_score)
        elif audience_score == -1:
            rating_text = "has a \002%d%%\002 critic score" % \
                (critics_score)
        else:
            rating_text = "has a \002%d%%\002 critic and \002%d%%\002 " \
                "audience score" % (critics_score, audience_score)
        irc.privmsg(channel,
                    "%s: [%d/%d] \037%s\037%s starring %s %s according"
                    " to RottenTomatoes." %
                    (sender.nick, rt_idx + 1, rt_data['total'],
                        title, year_text, cast, rating_text))
    except:
        irc.privmsg(channel,
                    "%s: Error looking up that movie." % (sender.nick))
        rt_data = None
        return
