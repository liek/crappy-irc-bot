# Port of hal.tcl (Eggdrop "learn" script)
# Orginially by Hal9000 @ irc.ptnet.org

import time
from collections import defaultdict
import operator
import os
import re
import shutil

plugin = None
hal_doc = "learn.dat"
hal_log = "learn.log"
channels = ["#liek"]
flood_list = {}

m_learn = re.compile(r'^\s*(\w+)(?:\s+'
                     r'("(?:\s+[^>"]+?|[^\s>"][^>"]*?)"(?!\S)|[^\s">]+)?)?'
                     r'(?:\s+([^\s].*))?')
m_ri = re.compile(r'^(\d+)\s+([^\s].*)')


def init(p, bot):
    global plugin, hal_doc, channels, hal_log
    plugin = p
    irc = bot

    plugin.bind_public('??', handle_query, True)
    plugin.bind_public('**', handle_search, True)
    plugin.bind_public('!learn', handle_learn, False)

    try:
        hal_doc = irc.cfg.get('hal', 'dat_file')
    except:
        pass
    try:
        channels = irc.cfg.get('hal', 'channels').split()
    except:
        pass
    try:
        hal_log = irc.cfg.get('hal', 'log_file')
    except:
        pass

    if not os.path.exists(hal_doc):
        try:
            db = open(hal_doc, 'w', encoding='utf-8')
            db.write("Hal9000 ?? Just do a \x02?? \x1fword\x1f\x02 or \x02?? "
                     "\x1fword\x1f > \x1fnick\x1f\x02\n")
            db.close()
        except:
            plugin.logger.info("Error creating hal database")

    plugin.logger.info("hal.py by Matt@#liek loaded.")


def handle_query(irc, sender, channel, args):
    global flood_list

    if irc.irclower(channel.channel) not in channels:
        # ignore -> we don't work here
        return

    if len(args) < 2:
        send_helpmsg(irc, sender.nick, "??")
        return

    term = args[1].lower()
    target = irc.irclower(channel.channel)
    changed_target = 0

    if len(args) >= 3:
        for param in range(2, len(args) - 1):
            if args[param].startswith('>'):
                if param == len(args) - 1:
                    send_helpmsg(irc, sender.nick, "??")
                    return
                target = irc.irclower(args[param + 1])
                changed_target = 1
                break
        if changed_target:
            term = ' '.join(args[1:param]).lower()
        else:
            term = ' '.join(args[1:]).lower()
    #term_escaped = term_escape(term)

    if (term, target) in flood_list:
        if int(time.time()) - flood_list[(term, target)] > 60:
            del flood_list[(term, target)]
        else:
            if not channel.isop(sender.nick):
                irc.privmsg(channel,
                            "\002Hey %s, I've already told you about "
                            "\"%s\"... no need to repeat (I think)\002" %
                            (sender.nick, term))
                return
    flood_list[(term, target)] = int(time.time())

    try:
        definitions = get_definitions(term)
    except Exception as e:
        irc.privmsg(channel, str(e))

    # XXX: Here we should be checking who we're sending to

    if len(definitions) < 1:
        irc.privmsg(target,
                    get_output(term, 'x', 'No definition found for word.'))
    elif len(definitions) == 1:
        irc.privmsg(target, get_output(term, None, definitions[0][2]))
    else:
        count = 0
        for i in definitions:
            count += 1
            irc.privmsg(target, get_output(term, count, i[2]))


def handle_search(irc, sender, channel, args):
    if irc.irclower(channel.channel) not in channels:
        return

    if len(args) < 2:
        irc.privmsg(channel, "Not enough arguments.")
        return

    search = ' '.join(args[1:]).lower()
    start_clock = time.clock()

    try:
        matches = term_search(search)
    except Exception as e:
        irc.privmsg(channel, str(e))
        return

    time_elapsed = time.clock() - start_clock

    if matches is None:
        irc.privmsg(channel,
                    "\002Too many matches.\002 Try a more complex search. "
                    "(\002\037%s\037s\002)" %
                    round(time_elapsed, 3))
        return
    if len(matches) > 0:
        idx = 0
        while idx < len(matches):
            matches[idx] = ("\037%s\037" % matches[idx])
            idx += 1
        irc.privmsg(channel,
                    ("\002Found \037%d\037 matches.\002 Sorted: %s. "
                        "(\002\037%s\037s\002)" %
                        (len(matches), term_unescape(', '.join(matches)),
                         round(time_elapsed, 3))))
    else:
        irc.privmsg(channel,
                    "Sorry, no matches for %s in the definition database. "
                    "(\002\037%s\037s\002)" %
                    (search, round(time_elapsed, 3)))


def handle_learn(irc, sender, channel, args):
    if len(args) < 2:
        return
    if irc.irclower(channel.channel) not in channels:
        return
    try:
        command, term, data = m_learn.match(args[1]).groups()
    except:
        irc.notice(sender.nick,
                   "Syntax: !learn (add, rem, ins, rep) term [index] "
                   "[definition]")
        return

    if command == 'stats':
        try:
            stats = get_stats()
        except:
            return
        irc.privmsg(channel,
                    "\002Stats\002: Terms: \037%d\037 "
                    "Definitions: \037%d\037 "
                    "Most Definitions: \002%s\002 (\037%d\037) "
                    "Most Authored: \002%s\002 (\037%d\037)" % (
                        stats['terms'], stats['definitions'],
                        stats['most_defs'], stats['most_defs_count'],
                        stats['most_written'], stats['most_written_count'])
                    )
        return

    # XXX: Needs an auth system, but for now we'll go for chanops
    if not channel.isop(sender.nick):
        return

    if term is not None:
        term = ' '.join(term.replace('"', '').lower().split())
    if data is not None:
        data = data.rstrip()

    if command == 'add':
        if term is None or data is None:
            send_helpmsg(irc, sender.nick, 'add')
            return
        try:
            add_term(sender, term, data)
        except Exception as e:
            irc.notice(sender.nick, str(e))
            return
        irc.notice(sender.nick, "Definition added.")

    elif command == 'rem':
        if term is None:
            send_helpmsg(irc, sender.nick, 'rem')
            return
        if data:
            idx = data.split(None, 1)[0]
            if not idx.isdigit():
                irc.notice(sender.nick,
                           "Invalid index, must be an integer")
                return
            idx = int(idx)
        else:
            idx = 0
        try:
            removed = remove_term(sender, term, idx)
        except Exception as e:
            irc.notice(sender.nick, str(e))
            return
        if removed:
            irc.notice(sender.nick, "%d definition%s removed." %
                       (removed, 's' if removed > 1 else ''))
        else:
            irc.notice(sender.nick,
                       "Couldn't find that word in my dictionary.")

    elif command == 'ins' or command == 'rep':
        # !learn ins/rep word idx def
        if term is None or data is None:
            send_helpmsg(irc, sender.nick, command)
            return
        try:
            idx, definition = m_ri.match(data).groups()
        except:
            send_helpmsg(irc, sender.nick, command)
            return

        idx = int(idx)
        if idx < 1:
            irc.notice(sender.nick, "Index starts at 1")
            return

        if command == 'ins':
            try:
                insert_term(sender, term, idx, definition)
            except Exception as e:
                irc.notice(sender.nick, str(e))
                return
            irc.notice(sender.nick, "Definition inserted")

        elif command == 'rep':
            try:
                replaced = replace_term(sender, term, idx, definition)
            except Exception as e:
                irc.notice(sender.nick, str(e))
                return
            if replaced:
                irc.notice(sender.nick, 'Definition replaced.')
            else:
                irc.notice(sender.nick,
                           "I couldn't find that definition in my dictionary.")
    else:
        send_helpmsg(irc, sender.nick)


def get_definitions(term):
    term_escaped = term_escape(term)
    try:
        fh = open(hal_doc, encoding='utf-8')
    except IOError:
        raise Exception('Error opening definition file!')

    matched_lines = []

    for line in fh:
        l = line.rstrip()
        split_line = l.split(None, 2)

        if len(split_line) < 3:
            continue
        if split_line[1] == term_escaped:
            matched_lines.append(split_line[:])
    fh.close()
    return matched_lines


def term_search(search, max_matches=10):
    try:
        fh = open(hal_doc, encoding='utf-8')
    except IOError:
        raise Exception('Error opening definition file!')

    matched_terms = []

    for line in fh:
        l = line.rstrip()
        split_line = l.split(None, 1)

        if len(split_line) < 2:
            continue

        term = split_line[1].split(None, 1)[0]
        term = term.lower()
        split_line[1] = split_line[1].lower()

        if term in matched_terms:
            continue

        if search in split_line[1]:
            if len(matched_terms) >= max_matches:
                fh.close()
                return None
            matched_terms.append(term)
    fh.close()

    if len(matched_terms) > 0:
        matched_terms.sort()
    return matched_terms


def add_term(sender, term, definition):
    term_escaped = term_escape(term)
    inserted = False
    count = 1
    try:
        db = HalDBEditor(hal_doc)
        for line in db.get():
            line = line.rstrip()
            if not inserted:
                _, line_term, _ = line.split(None, 2)
                line_term = line_term.lower()
                line_term = term_unescape(line_term)
                if line_term == term:
                    count += 1
                if line_term > term:
                    db.put("%s %s %s" %
                           (sender.nick, term_escaped, definition))
                    inserted = True
                    hal_logger('add', sender, term, count, definition,
                               save=True)
            db.put(line)
        if not inserted:
            db.put("%s %s %s" % (sender.nick, term_escaped, definition))
            hal_logger('add', sender, term, count, definition, save=True)
        db.close()
    except Exception:
        hal_logger('error', sender, "Error writing to dictionary file. (add)",
                   save=True)
        raise
    return True


def remove_term(sender, term, idx):
    term_escaped = term_escape(term)
    count = 0
    found = False
    removed = 0
    try:
        db = HalDBEditor(hal_doc)
        for line in db.get():
            if not found:
                _, line_term, _ = line.split(None, 2)
                line_term = line_term.lower()
                if line_term == term_escaped:
                    count += 1
                    if count == idx:
                        found = True
                    if found or idx == 0:
                        removed += 1
                        hal_logger('rem', sender, term, count, line)
                        continue
            db.put(line)
        db.close()
    except Exception:
        hal_logger('error', sender, "Error writing to definition file. (rem)",
                   save=True)
        raise
    hal_savelog()
    return removed


def replace_term(sender, term, idx, definition):
    term_escaped = term_escape(term)
    replaced = False
    count = 0
    try:
        db = HalDBEditor(hal_doc)
        for line in db.get():
            if not replaced:
                _, line_term, _ = line.split(None, 2)
                line_term = line_term.lower()
                if line_term == term_escaped:
                    count += 1
                    if count == idx:
                        db.put("%s %s %s" %
                               (sender.nick, term_escaped, definition))
                        replaced = True
                        hal_logger('rep', sender, line, term, count,
                                   definition, save=True)
                        continue
            db.put(line)
        db.close()
    except Exception:
        hal_logger('error', sender, "Error opening dictionary file. (rep)",
                   save=True)
        raise
    return replaced


def insert_term(sender, term, idx, definition):
    # XXX: We assume the file is sorted properly.
    # If it isn't: your fault when things go wrong :)
    term_escaped = term_escape(term)
    count = 0
    inserted = False
    try:
        db = HalDBEditor(hal_doc)
        for line in db.get():
            if not inserted:
                _, line_term, _ = line.split(None, 2)
                line_term = line_term.lower()
                line_term_unescaped = term_unescape(line_term)
                if line_term == term_escaped:
                    count += 1
                if count >= idx or line_term_unescaped > term:
                    db.put("%s %s %s" %
                           (sender.nick, term_escaped, definition))
                    inserted = True
                    hal_logger('ins', sender, term, idx, definition, save=True)
            db.put(line)
        if not inserted:
            db.put("%s %s %s" % (sender.nick, term_escaped, definition))
            hal_logger('ins', sender, term, idx, definition, save=True)
        db.close()
    except Exception:
        hal_logger('error', sender, "Error opening dictionary file. (ins)",
                   save=True)
        raise
    return True


def get_stats():
    try:
        fh = open(hal_doc, encoding="utf-8")
    except IOError:
        raise Exception('Error opening definition file!')
        return

    terms = defaultdict(int)
    authors = defaultdict(int)
    total = 0

    for line in fh:
        l = line.rstrip()
        split_line = l.split(None, 2)

        if len(split_line) < 3:
            continue

        terms[split_line[1].lower()] += 1
        authors[split_line[0].lower()] += 1
        total += 1
    fh.close()

    most_defs = max(iter(list(terms.items())), key=operator.itemgetter(1))[0]
    most_written = max(iter(list(authors.items())),
                       key=operator.itemgetter(1))[0]

    return {'terms': len(terms), 'definitions': total,
            'most_defs': term_unescape(most_defs),
            'most_defs_count': terms[most_defs],
            'most_written': most_written,
            'most_written_count': authors[most_written]}

help_messages = {
    None: "Syntax: !learn (add, rem, ins, rep) term [index] [definition]",
    '??': "Try ?? word || ?? word > target",
    'add': "Try !learn add term definition",
    'rem': "Try !learn rem term [idx]",
    'ins': "Try !learn ins term idx definition",
    'rep': "Try !learn rep term idx definition",
}


def send_helpmsg(irc, target, cmd=None):
    if cmd in help_messages:
        irc.notice(target, help_messages[cmd])


def hal_logger(action=None, sender=None, *args, **kwargs):
    if action:
        hal_logger.events.append({'action': action,
                                  'sender': sender,
                                  'time': time.localtime(),
                                  'args': args})
    if 'save' in kwargs and kwargs['save'] is True:
        if len(hal_logger.events) < 1:
            return
        log_fh = None
        try:
            log_fh = open(hal_log, 'a', encoding='utf-8')
        except:
            return

        sender = ""
        for e in hal_logger.events:
            if not sender:
                log_fh.write("%s : %s!%s@%s\n" %
                             (time.strftime("%a %b %d %H:%M:%S %Y", e['time']),
                              e['sender'].nick, e['sender'].username,
                              e['sender'].host))
                sender = e['sender'].nick
            elif sender != e['sender'].nick:
                log_fh.write("-\n")
                log_fh.write("%s : %s!%s@%s\n" %
                             (time.strftime("%a %b %d %H:%M:%S %Y", e['time']),
                              e['sender'].nick, e['sender'].username,
                              e['sender'].host))
            if e['action'] == 'add':
                log_fh.write(('Added: "%s"[%d] %s\n' %
                              (e['args'][0], e['args'][1], e['args'][2])))
            elif e['action'] == 'rem':
                log_fh.write(('Removed: "%s"[%d] %s\n' %
                              (e['args'][0], e['args'][1], e['args'][2])))
            elif e['action'] == 'ins':
                log_fh.write(('Inserted: "%s"[%d] %s\n' %
                              (e['args'][0], e['args'][1], e['args'][2])))
            elif e['action'] == 'rep':
                log_fh.write(('Replaced: "%s"[%d] %s With: %s\n' %
                              (e['args'][1], e['args'][2], e['args'][0],
                               e['args'][3])))
            elif e['action'] == 'error':
                log_fh.write(('Error: %s\n' % (e['args'][0])))
        log_fh.write("-\n")
        log_fh.close()
        hal_logger.events = []

hal_logger.events = []


def hal_savelog():
    hal_logger(save=True)


def get_output(word, index, definition):
    if index:
        return ("\002\037%s[\037\002%s\002\037]\037\002: %s" %
                (word, index, definition))
    else:
        return "\002\037%s\037\002: %s" % (word, definition)


def clear_flood(term, target):
    global flood_list
    flood_list.remove((term, target))


def term_unescape(term):
    unescaped = term.replace('%20', ' ')
    unescaped = unescaped.replace('%25', '%')
    return unescaped


def term_escape(term):
    escaped = term.replace('%', '%25')
    escaped = escaped.replace(' ', '%20')
    return escaped


class HalDBEditor():
    """This is a simple class to replace fileinput, which doesn't support
    string decoding when using inplace mode."""
    def __init__(self, db_fn):
        self._fn = db_fn
        self._fn_backup = db_fn + ".bak"
        self._db, self._db_bak = self._open_db()

    def _open_db(self):
        try:
            db_file = open(self._fn, 'r+', encoding='utf-8')
        except IOError:
            raise Exception("hal: Unable to open database")
        try:
            db_backup = open(self._fn_backup, 'w+', encoding='utf-8')
        except IOError:
            db_file.close()
            raise Exception("hal: Unable to open database backup")

        shutil.copyfileobj(db_file, db_backup)
        db_file.seek(0)
        db_file.truncate(0)
        db_backup.seek(0)
        return db_file, db_backup

    def get(self, strip_newline=True):
        while True:
            line = self._db_bak.readline()
            if not line:
                return
            if strip_newline:
                line = line.rstrip()
            yield line

    def put(self, line, add_newline=True):
        if add_newline:
            self._db.write(line + '\n')
        else:
            self._db.write(line)

    def close(self, keep_backup=False):
        try:
            self._db.close()
            self._db_bak.close()
            if not keep_backup:
                os.unlink(self._fn_backup)
        except:
            pass
        self._db = None
        self._db_bak = None
