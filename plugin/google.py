# simple google lookup

import lxml.html

import urllib.request
from urllib.parse import quote_plus

plugin = None


def init(p, bot):
    global plugin
    plugin = p
    plugin.bind_public('!google', google, False)


def google(irc, sender, channel, args):
    if len(args) < 2:
        return
    query = quote_plus(args[1])
    my_url = "http://www.google.com/search?q=%s&btnI=" % (query)

    try:
        opener = urllib.request.build_opener()
        opener.addheaders = [('User-agent', 'Mozilla/5.0'),
                             ('Referer', 'http://www.google.com/')]
        page = opener.open(my_url)
        page_url = page.geturl()
        p = lxml.html.parse(page)
        title = p.find(".//title").text
        #clean_title = title.decode('utf-8', 'ignore')
        clean_title = title.strip()
        clean_title = clean_title.replace('\r', '')
        clean_title = clean_title.replace('\n', '')
        clean_title = clean_title.replace('\t', '')
        irc.privmsg(channel, "%s - %s" % (clean_title, page_url))
    except:
        irc.privmsg(channel, "%s: Failed" % sender.nick)
