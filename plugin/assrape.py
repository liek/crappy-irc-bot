import random

plugin = None
assrape_list = []


def init(p, bot):
    global plugin
    plugin = p
    irc = bot
    plugin.bind_public('~assrape', assrape, True, 1)
    load_list()

    plugin.logger.info("assrape.py v2.0 by Matt@#liek loaded.")


def assrape(irc, sender, channel, args):
    global assrape_list
    if len(args) > 2 and args[1].lower() == '-add':
        if channel.isop(sender.nick):
            found = False
            item = args[2].rstrip()
            if item[0] == ':' and len(item) < 2:
                irc.privmsg(channel, "I can't do that.")
            item_l = item.lower()
            for i in assrape_list:
                if i.lower() == item_l:
                    irc.privmsg(channel,
                                "I've already got one of those.")
                    found = True
            if not found:
                add_list(args[2])
                irc.privmsg(channel, '"%s" added.' % (item))
    elif len(args) > 2 and args[1].lower() == '-rem':
        if channel.isop(sender.nick):
            found = False
            if not assrape_list:
                irc.privmsg(channel, 'My bag is empty.')
                return
            item = args[2].lower()
            for x in range(len(assrape_list)):
                if assrape_list[x].lower() == item:
                    assrape_list.pop(x)
                    save_list()
                    found = True
                    irc.privmsg(channel, '"%s" Removed.' % (args[2]))
                    break
            if not found:
                irc.privmsg(channel, "I couldn't find that.")
    elif len(args) > 1 and args[1].lower() == '-reload':
        if channel.isop(sender.nick):
            load_list()
            irc.privmsg(channel, "Reloaded assrape list.")
    elif len(args) > 1:
        nick = args[1]
        if irc.irccmp(irc.nickname, nick) == 0:
            irc.privmsg(channel, "Nice try, asshole.")
            irc.kick(channel, sender.nick, "Assraped")
        elif channel.isop(sender.nick):
            u = channel.get_user(nick)
            if u:
                item = None
                try:
                    item = random.choice(assrape_list)
                except IndexError:
                    pass
                if not item:
                    item = ':something'
                assrape_with = ""
                if item[0] != ':':
                    assrape_with = ('an ' if item[0].lower() in 'aeiou'
                                    else 'a ') + item
                else:
                    assrape_with = item[1:]
                irc.action(channel,
                           "shoves %s up %s's ass." %
                           (assrape_with, u.nick))
                irc.kick(channel, u.nick, "Assraped with %s" %
                         assrape_with)
    else:
        irc.notice(sender.nick, "Syntax: ~assrape <nick>")
        if channel.isop(sender.nick):
            irc.notice(sender.nick, "        ~assrape -add [:]<item>")
            irc.notice(sender.nick, "        ~assrape -rem [:]<item>")


def load_list():
    global assrape_list
    assrape_list = []
    try:
        with open('assrape.txt', 'r', encoding='utf-8') as f:
            assrape_list = f.read().splitlines()
    except:
        pass


def add_list(item):
    global assrape_list
    assrape_list.append(item)
    try:
        with open('assrape.txt', 'a', encoding='utf-8') as f:
            f.write(item + "\n")
    except:
        pass


def save_list():
    try:
        with open('assrape.txt', 'w', encoding='utf-8') as f:
            for i in assrape_list:
                f.write(i + "\n")
    except:
        pass
