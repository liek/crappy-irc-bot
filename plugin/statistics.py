# stats script
# requires config.stats to be defined

plugin = None


def init(p, bot):
    global plugin
    plugin = p
    plugin.subscribe('kick', kick)
    plugin.bind_public('!kickstat', kickstat)
    plugin.bind_public('!kstat', kstat)


def kick(irc, sender, channel, nick, message):
    try:
        irc.cfg.set('stats', nick, irc.cfg.getint('stats', nick) + 1)
    except KeyError:
        irc.cfg.set('stats', nick, 1)
    plugin.logger.info("someone got kicked: %s" % nick)


def kickstat(irc, sender, channel, args):
    irc.privmsg(channel, irc.cfg.items('stats'))


def kstat(irc, sender, channel, args):
    try:
        name = args[1]
    except:
        name = ""
    try:
        kicks = irc.cfg.get('stats', name)
    except KeyError:
        kicks = 0
    irc.privmsg(channel, "%s: %i" % (name, kicks))
