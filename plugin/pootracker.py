import datetime
from dateutil import tz
#import json
import sqlite3
import os
import re
from random import choice

plugin = None

pt_db = None
pootracker_file = "pootracker.db"

pootrackerupdate_url = 'http://www.liek.net/cgi-bin/mnews/plstats.cgi'
pootrackerupdate_login = ""
pootrackerupdate_password = ""


def init(p, bot):
    global plugin, pootracker_file
    global pootrackerupdate_login, pootrackerupdate_password

    plugin = p
    plugin.bind_public('!biggestpoop', display_biggest_poops)
    plugin.bind_public('!popularpoops', display_popular_poops)
    plugin.bind_public('!mypoops', display_user_poop)
    plugin.bind_public('!poop', poop)
    plugin.bind_public('!pooptz', pooptz)

    irc = bot

    try:
        pootrackerupdate_login = irc.cfg.get('pootracker', 'update_login')
    except:
        pass
    try:
        pootrackerupdate_password = irc.cfg.get('pootracker',
                                                'update_password')
    except:
        pass


def open_db():
    global pt_db

    # don't even care about race conditions. fuck you
    create_table = not os.path.exists(pootracker_file)

    pt_db = sqlite3.connect(pootracker_file, check_same_thread=False)

    # make that shit
    if create_table:
        c = pt_db.cursor()
        c.execute("""CREATE TABLE poop (id INTEGER PRIMARY KEY, user_id INT,
                  score INT, deuce_date DATETIME, reaction TEXT);""")
        c.execute("""CREATE TABLE users (id INTEGER PRIMARY KEY, name TEXT,
                  tz_offset INTEGER);""")
        c.execute("""CREATE TABLE hosts (id INTEGER PRIMARY KEY, host TEXT,
                  user_id INTEGER,
                  FOREIGN KEY(user_id) REFERENCES users(id));""")
        pt_db.commit()
        c.close()


def get_user_id(sender):
    global pt_db

    if pt_db is None:
        return -1

    c = pt_db.cursor()
    c.execute("""SELECT u.id FROM users AS u JOIN hosts AS h
              ON u.id = h.user_id WHERE h.host = ?""",
              (sender.get_usermask(),))

    if not c:
        return -1

    try:
        user_id = int(c.fetchone()[0])
    except:
        return -1

    return user_id


def display_biggest_poops(irc, sender, channel, args):
    global pt_db

    open_db()
    c = pt_db.cursor()

    c.execute("""SELECT u.name, COUNT(p.score) AS a FROM poop AS p
              JOIN users AS u ON u.id = p.user_id
              GROUP BY u.name ORDER BY a DESC LIMIT 5""")

    i = 1  # counter

    irc.privmsg(channel, "The Biggest Push -> Top 5 Total Poops")

    for u in c.fetchall():
        if u[0] is None:
            break

        irc.privmsg(channel, "%s. \002%s\002: %s" % (i, u[0], int(u[1])))
        i += 1

    c.execute("""SELECT u.name, AVG(p.score) AS a FROM poop AS p
              JOIN users AS u ON u.id = p.user_id
              GROUP BY u.name ORDER BY a DESC LIMIT 5""")

    i = 1

    irc.privmsg(channel, "The Biggest Poop -> Top 5 Highest Average")
    for u in c.fetchall():
        if u[0] is None:
            break

        irc.privmsg(channel, "%s. \002%s\002: %s" % (
                    i,
                    u[0],
                    "{0:.2f}".format(float(u[1]))))
        i += 1
    pt_db.close()


def display_popular_poops(irc, sender, channel, args):
    global pt_db
    open_db()
    c = pt_db.cursor()

    c.execute("""SELECT p.score as score, COUNT(p.score) as `count` FROM poop
              AS p GROUP BY p.score ORDER BY `count` DESC LIMIT 5""")
    irc.privmsg(channel, "The Most Popular Poops")
    i = 1
    for u in c.fetchall():
        if u[0] is None:
            break
        irc.privmsg(channel, "%s. \002%s/10\002: %s poop(s)" %
                    (i, u[0], int(u[1])))
        i += 1
    pt_db.close()


def display_user_poop(irc, sender, channel, args):
    global pt_db
    open_db()

    user_id = get_user_id(sender)

    if user_id < 1:
        irc.privmsg(channel, "You haven't took a shit yet. Get on it.")
        return

    c = pt_db.cursor()

    c.execute("""SELECT p.score, p.deuce_date, u.tz_offset FROM poop AS p
              JOIN users AS u ON p.user_id = u.id WHERE p.user_id = ?
              ORDER BY p.deuce_date ASC""", (user_id,))

    res = c.fetchall()

    total = len(res)

    if total == 0:
        irc.privmsg(channel, "You haven't took a shit yet. Get on it.")
        return

    summation = 0
    last_deuce = None
    tz_offset = None

    for r in res:
        summation += int(r[0])
        try:
            last_deuce = float(r[1])
        except:
            last_deuce = 0.0
        tz_offset = int(r[2])

    avg = summation / float(total)

    if last_deuce:
        last_deuce = datetime.datetime.fromtimestamp(last_deuce, tz.tzlocal())
        last_deuce += datetime.timedelta(hours=tz_offset)
        last_deuce = last_deuce.strftime("%m/%d/%y %I:%M%p")

    irc.privmsg(
        channel,
        "%s: \002Total\002: %s / \002Avg\002: %.2f / \002Recent\002: %s" %
        (sender.nick, total, avg, last_deuce))

    pt_db.close()


def save_user_poop(irc, channel, sender, rating, reaction=''):
    global pt_db

    # see if input was entered as N/10
    rating_ratio = re.match(r'(\d)/\d', rating)

    if rating_ratio:
        rating = int(rating_ratio.groups()[0])

    try:
        rating = int(rating)
    except:
        irc.privmsg(channel,
                    "Can't understand that shit! Shit then enter a number.")
        return

    if rating < 0 or rating > 10:
        irc.privmsg(channel,
                    "You took an impossible shit. I don't believe you.")
        return

    if "phix" in sender.nick.lower() or "supdawgs" in sender.username.lower():
        irc.privmsg(channel, "dc")
        return

    # using rating
    user_id = get_user_id(sender)

    if user_id < 1:
        user_id = set_user_timezone(sender, '-8')

    current_time = datetime.datetime.utcnow().strftime("%s")

    c = pt_db.cursor()
    c.execute("""INSERT INTO poop (user_id, score, deuce_date, reaction)
              VALUES (?, ?, ?, ?)""",
              (user_id, rating, current_time, reaction))

    pt_db.commit()

    poop_message = choice(["Stored that shit", "Logged your log",
                           "Committed your ... shitted?", "Saved that dookie"])
    # this is where i make fun of python's string interpolation
    irc.privmsg(channel, "%s: %s => %s" % (sender.nick, poop_message, rating))


# sets the user timezone and creates a user if it doesn't already exist
def set_user_timezone(sender, timezone):
    global pt_db

    user_id = get_user_id(sender)

    c = pt_db.cursor()

    if user_id < 1:
        # register the user first
        c.execute("INSERT INTO users (name, tz_offset) VALUES (?, ?)",
                  (sender.nick, timezone))
        c.execute("SELECT last_insert_rowid()")
        user_id = int(c.fetchone()[0])

        c.execute("INSERT INTO hosts (host, user_id) VALUES (?, ?)",
                  (sender.get_usermask(), user_id))
    else:
        c.execute("UPDATE users SET tz_offset = ? WHERE id = ?",
                  (timezone, user_id))

    pt_db.commit()

    return user_id


def poop(irc, sender, channel, args):
    global pt_db
    # rate that shit. literally. betrayed sucks. so does sloat. and tom.

    if len(args) < 2:
        irc.privmsg(channel,
                    "You didn't give me anything. Can't rate that shit!")
        return

    rating = args[1]

    reaction = ''

    if len(args) > 2:
        reaction = ' '.join(args[2:])

    open_db()
    save_user_poop(irc, channel, sender, rating, reaction)
    pt_db.close()


def pooptz(irc, sender, channel, args):
    if len(args) < 2:
        irc.privmsg(
            channel,
            "You didn't give me anything. Can't set that timezone shit!")
        return

    try:
        timezone = int(args[1])
    except:
        irc.privmsg(channel, "You didn't give me a valid offset.")
        return

    if timezone < -12 or timezone > 14:
        irc.privmsg(channel,
                    "You didn't give me a valid offset. (-12 to +14)")
        return

    open_db()
    set_user_timezone(sender, timezone)
    pt_db.close()

    irc.privmsg(channel, "%s: Updated your time zone." % (sender.nick))
