import random

hot = {
    'default': [
        "Not recommended, but you could sweat your balls off.",
        "Your car now doubles as a sauna.  Beware large men in towels.",
        "9/10 people die of heat exhaustion on a day like this.",
        "BIKINI WEATHER!",
        "Beach.",
        "Isn't the desert nice this time of year?",
        "Don't own a grill? Just use the driveway.",
        "Do you have life insurance?",
        "Drink water or die.",
        "I burned my feet walking on grass.",
        "I just saw a fucking bird panting.",
        "I need gloves to touch the steering wheel.",
        "I recommend staying away from fat people.",
        "I'll be in the fridge.",
        "It's hotter outside than my fever.",
        "Keeping the AC business in business.",
        "Like super models, IT'S TOO FUCKING HOT.",
        "Lock up yo' ice cream trucks, lock up yo' wife.",
        "POOOOOOOOOOOOL!",
        "SWIMMIN HOLE!",
        ("Looked in the fridge this morning for some eggs. They're already"
         " cooked."),
        "Not even PAM can make me not stick to this seat",
        "So hot my dog developed sweat glands.",
        "sweating my balls off",
        "uncontrollable swamp-ass",
        "...in bed!",
        "NO SHIT.",
        "WHAT? I COULDN'T FUCKING HEAR YOU OVER ALL THIS SWEAT.",
        "WHY DOES THE FUCKING SHOWER ONLY HAVE HOT WATER TODAY!?",
        "Time to crank the AC.",
        "At least it's not humid!  Unless, ya' know... it's humid.",
        "FUCKING SUNBURNED, AND I WAS INSIDE ALL DAY.",
        "HOLY FUCK",
        "Hot dog weather!  Hot everything weather! IT'S FUCKING HOT!",
        "Still not appropriate to wear a speedo though.",
        "TAKE IT OFF!",
        "times you wish you didn't have leather seats",
        "Better than winter.",
        "THE FUCKING EQUATER CALLED, AND IT'S JEALOUS.",
        "Why, oh why did we decide to live in an oven?",
    ],
    'rain': [
        "AND WET",
        "NOT A FUCKING DROP TO DRINK",
        "I LOST MY FUCKING UMBRELLA",
        "Maybe it'll cool off?",
        "FUCKING HYDROPLANE",
        "Wasn't it supposed to rain? Oh wait.",
    ],
    'snow': [
        "APOCALYPSE CURRENTLY HAPPENING THIS MOMENT",
    ],
    'cloudy': [
        "THE SUN FUCKING SUCKS",
        "I'd practice karate if I could",
        "Back to the tanning booth I guess."
        "This is literally the worst day EVER",
        "TIME FOR SOME BAD DECISIONS",
    ],
    'storm': [
        "RIDE THE FUCKING LIGHTNING",
        "Kite flyin' weather! Right?",
        "KEEP AWAY FROM WINDOWS... 8",
        "It'll get cooler after this -- or else",
        "FUCKING OBAMA",
        "I'LL CHASE THIS STORM OUT OF HERE",
    ],
    'clear-day': [
        "MORE FUCKING SUN?!?",
        "AM I COOKING?",
        "I can't concentrate in this condition",
        "Charcoal BBQ inside!",
        "I'M TURNING IN TO A RAISIN",
        "EVEN SUN DRIED TOMATOES HATE THIS",
    ],
    'clear-night': [
        "IT'S STILL HOT. FUCK THIS.",
        "Time for some freeze pops",
        "LOOK AT THE FUCKING STARS",
        "We'll have to wait til tomorrow to see if the sun burned out",
    ],
    'sleet': [
        "WTFUCK?",
    ],
    'fog': [
        "What? This isn't San Fransisco, is it?",
        "I CAN'T FUCKING SEE ANYTHING, EVEN WITH MY EYES OPEN",
        "I just got Lasik to see the landscape, and now this.",
        "AT LEAST NO ONE CAN SEE MY UGLY SHOES",
    ],
    'partly-cloudy-day': [
        "My house turned in to a sweat lodge",
        "Well, at least we don't live in Cleveland",
        "PUT ON YOUR FUCKING SUNDAY BEST",
        "DON'T FORGET YOUR RADIATION SUIT",
    ],
    'partly-cloudy-night': [
        "THE MOON KEEPS DISAPPEARING!",
        "Is this the new normal?",
        "STILL CAN'T FUCKING GO OUTSIDE",
        "GIRL'S NIGHT OUT, BOY'S NIGHT OUT, OR SOMETHING",
    ],
    'wind': [
        "Is there a tornado outside?",
        "I wish I was in a hair metal band right now",
        "I CAN'T LIGHT MY FUCKING CIGARETTE",
        "THIS FUCKING BLOWS.... get it?",
        "This wind blew my pants off... yes... the wind...",
    ],
    'hail': [
        "A DIFFERENT FUCKING KIND OF ICE BALLS",
        "MY POOR HATCHBACK!",
        "GOOD. I NEEDED SOME FUCKING ICE FOR MY DRINK",
        "BETTER GO OUTSIDE FOR A CLOSER LOOK",
    ],
}
cold = {
    'default': [
        "Your source of crude weather!",
        "Where's a Tauntaun when you need one?",
        "Shrinkage's best friend.",
        "Why does my car feel like a pair of ice skates?",
        "WHAT THE FUCK DO YOU MEAN IT'S NICER IN ALASKA?",
        "I hear Siberia is the same this time of year.",
        "Fuck this place.",
        "I'm sorry.",
        "Nothing a few shots couldn't fix",
        "Fantastic do-nothing weather.",
        "I'm not sure how you can stand it",
        "FUCKING JOGGING WEATHER",
        "Freezing my balls off ",
        "Why do I live here?",
        "Really?",
        "Sock warmers are go.  Everywhere.",
        "Fresh off the tap.",
        "Call your local travel agency and ask them if they're serious.",
        "My nipples could cut glass",
        "Cold as... which is it again?",
        "GREAT! If you're a penguin.",
        ("Actually, a sharp-stick in the eye might not all be that bad right"
         " now."),
        "Wear a fucking jacket.",
        "Where's the cat? Oh shit. Fluffy's frozen.",
        "Good baby making weather.",
        "Blue balls x 2",
        "Anything wooden will make a good fireplace.  Thank us later.",
        "THO Season.",
        "Keep track of your local old people.",
        "Freezing my balls off out here",
        "You think this is cold? Have you been to upstate New York?",
        "I'm defrosting inside of my freezer.",
        "Should have gone south",
        "It's time for a vacation.",
        "Even penguins are wearing jackets.",
        "wang icicles.",
        "Warmer than Hoth.",
        "Put on some fucking socks.",
        "MOVE THE FUCK ON GOLDILOCKS",
        "It's a tit-bit nipplie.",
        "It's bone chilling cold out.  Sorry ladies.",
        ("Good news, food won't spoil nearly as fast outside.  Bad news,"
         " who cares?"),
    ],
    'rain': [
        "AND RAINING",
        "DIDN'T GUNS & ROSES HAVE SOMETHING TO SAY ABOUT THIS SHIT?",
        "I'M GONNA FUCKING CATCH A COLD",
        "I'D GO HOME IF MY HOME WASN'T FROZEN",
    ],
    'snow': [
        "AND SNOWING",
        "HELP! I CAN'T FUCKING SNOWBOARD!",
        "Better get the snow shovel.... in a minute.",
        "IT'S FUCKING MAGICAL OR SOMETHING",
        "TWO IDENTICAL SNOWFLAKES! BRING THEM INSIDE TO PRESERVE THEM!",
        "CALL VLADIMIR PUTIN TO HELP US!",
    ],
    'cloudy': [
        "Cold and Cloudy, this is the worst day ever",
        "IT BETTER NOT FUCKING SNOW",
        "FUCKING WINTER IS COMING... YESTERDAY",
        "It's not manic, it's the other one",
    ],
    'storm': [
        "OH NOW WE'RE IN FUCKING TROUBLE",
    ],
    'clear-day': [
        "THE SUN IS COLD. WHAT THE FUCK.",
        "My lips are turning blue... from this lollipop.",
        "George RR Martin always kills Starks in this kind of weather",
        "Start spreading the news, I'm leaving today",
        "SANTA'S FUCKING MOVING IN",
    ],
    'clear-night': [
        "I REGRET LIVING HERE",
        "TURN UP THE FUCKING HEAT",
        "HOLY FUCK THE TOILET SEAT IS COLD",
    ],
    'sleet': [
        "TINY ICE BALLS? HOW'D YOU KNOW MY NICKNAME?",
        "Better go back to sleet. Sleet... Sleep.",
        "STOP IT GOD DAMMIT!",
        "WHO LEFT THE FUCKING ICE MACHINE ON?",
    ],
    'fog': [
        "SOMETHING'S PROBABLY WRONG HERE",
    ],
    'partly-cloudy-day': [
        "WHY DO BAD THINGS HAPPEN TO GOOD FUCKING PEOPLE",
        "Welp, let's call it a day",
        "DON'T STOP FUCKING BELIEVING",
        "Actually, I like the cold",
    ],
    'partly-cloudy-night': [
        "SNUGGLE TIME",
        "Warm up by the Hearth. Hearth Ledger.",
        "I'll start a fire in here. Watch me!",
    ],
    'wind': [
        "I'M LOOTHING FEELING IN MY FUCKING LIPTH",
        "WIND CHILL? YES IT IS.",
        "EVEN NON-RAPISTS ARE WEARING SKI MASKS",
    ],
    'hail': [
        "WELL THIS IS FUCKING INTERESTING",
    ],
}

alright = {
    'default': [
        "WHAT THE FUCK EVER",
        "Listen, weather. We need to have a talk.",
        "Let's go to the beach!  In three months when it's nice again...",
        "I've seen better days",
        "An Eskimo would beat your ass to be here",
        "At least you aren't living in a small town in Alaska",
        "Better than a sharp stick in the eye.",
        "Today is the perfect size, really honey.",
        "OH NO. THE WEATHER MACHINE IS BROKEN.",
        "Inside or outside?  Either way it's still today.",
        "From inside it looks nice out.",
        "Compared to how awful it's been this is great!",
        "FUCKING NOTHING TO SEE HERE",
        "Well, at least we're not in prison.",
        "Might as well rain, I'm not going out in that.",
        "It could be worse.",
        "I love keeping the heat on for this long.",
        "Where life is mediocre",
        "Today makes warm showers way nicer.",
        "Maybe inviting the inlaws over will improve today.",
        "It's either only going to get better or worse from here!",
        "Is that the sun outside?  Why isn't it doing anything?",
        "If we go running maybe we won't notice.",
        "Everything's nice butter weather!",
        "Can't complain about today, but I want to!",
        "Slap me around and call me Sally.  It'd be an improvement.",
        "Maybe Jersey Shore is on tonight.",
        "If it's raining cats and dogs, hope you're not a pet person.",
        "Here's to making your blankets feel useful.",
    ],
    'rain': [
        'AND WET',
        "or maybe it's not alright",
        "ONLY 39 MORE DAYS OF THIS AND THE EARTH WILL BE CLEANSED",
        "I give up",
        "Don't go outside. Just don't.",
    ],
    'snow': [
        'AND SNOWING',
        "SOMETHING'S NOT FUCKING RIGHT",
        "You know nothing Jon Snow... except all those things you know",
    ],
    'cloudy': [
        "STOP FUCKING ASKING ME WHAT IT'S LIKE OUTSIDE",
        "It could be worse, right? Screw that",
        "WAIT A MINUTE?!",
        "KNOCK-KNOCK! I HATE YOU!",
    ],
    'storm': [
        "Alright? My ass.",
    ],
    'clear-day': [
        "Time for some fucking apple cider",
        "Well, at least it's not shitty",
        "Time to drink",
        "I can go outside without shoes? No? FUCK!",
    ],
    'clear-night': [
        "I GOT ALL DRESSED UP FOR FUCKING NOTHING",
        "Sure, I can wear that...WHEN IT'S WARM OUT!",
        "Time to keep drinking",
    ],
    'sleet': [
        "What?",
    ],
    'fog': [
        "At least it's not foggy",
        "I can't see my own ass",
    ],
    'partly-cloudy-day': [
        "That cloud ruined my day",
        "WHO MADE THIS SHIT HAPPEN",
        "I looked outside, and now I wanna drink",
    ],
    'partly-cloudy-night': [
        "Cancel the plans. Cancel EVERYTHING.",
        "Quit complaining. Oh wait, I just looked outside. Keep complaining",
        "I won't have my dreams ruined by this crap",
    ],
    'wind': [
        "Put your windscreen on your microphone, jerk",
        "Pissin' in the wind",
        "MOMMY MY FUCKIN EARS",
        ("If you just littered, it's gone now. Also you made that indian cry. "
         "Dick."),
    ],
    'hail': [
        "HOLY FUCKSHIT",
    ],
}

nice = {
    'default': [
        "...what?",
        ("Celebrate Today's Day and buy your Today a present so it knows you"
         " care."),
        "Enjoy.",
        "FUCKING AFFABLE AS SHIT",
        "FUCKING NOTHING WRONG WITH TODAY",
        "FUCKING SWEET",
        "Give the sun a raise!",
        "I approve of this message!",
        "I can't  believe it's not porn!",
        "I feel bad about playing on my computer all day.",
        "I made today breakfast in bed.",
        "IT'S ABOUT FUCKING TIME",
        "It is now safe to leave your home.",
        "It is safe to take your ball-mittens off.",
        ("It's that kind of day where you want zip off pants, until you"
         " realize how much of a jackass you look like in them."),
        "LETS HAVE A FUCKING PICNIC",
        "LETS HAVE A FUCKING SOIREE",
        "More please.",
        "OPEN A FUCKING WINDOW",
        "Operation beach volleyball is go.",
        "PUT A FUCKING CAPE ON TODAY, BECAUSE IT'S SUPER",
        "Party in the woods.",
        "Plucky ducky kinda day.",
        "Quit your bitching",
        "READ A FUCKIN' BOOK",
        "STOP AND SMELL THE FUCKING ROSES",
        "Sunglasses on, and the cool train is about to depart.",
        ("The geese are on their way back!  Unless you live where they"
         " migrate to for the winter."),
        "There are no rules today, blow shit up!",
        "Today called just to say \"Hi.\"",
        "Today is better than an original holographic charizard.  Loser!",
        ("Today is like \"ice\" if it started with an \"n\". Fuck you, we"
         " don't mean nce."),
        "WOO, Spring Break!",
        "Water park!  Water drive!  Just get wet!",
        "What would you do for a holyshititsniceout bar?",
        "uh, can we trade?",
    ],
    'rain': [
        "AND WET",
        "Nice? Well, it's not raining fire.",
        "Is this ass-head rain?",
        "GLAD I WORE A DIAPER TODAY... what?",
    ],
    'snow': [
        "HOLY SHIT",
    ],
    'cloudy': [
        "Who needs the sun? That's what I say.",
        "I FUCKING LIKE CLOUDS",
        "It could be nicer. Do you hear me weather?",
    ],
    'storm': [
        "THIS DOESN'T EVEN MAKE SENSE",
    ],
    'clear-day': [
        "Well, clearly it's a nice day. (see what I did there?)",
        "WEATHER! FUCK YEAH!",
        "OH SHIT NICE!",
        "THIS IS THE GREATEST FUCKIN DAY EVER",
        "I'M GOING OUTSIDE AND YOU CAN'T FUCKING STOP ME",
        "THIS FUCKING RULES",
    ],
    'clear-night': [
        "KEEP THE FUCKING WINDOWS OPEN",
        "PANTS OFF",
        ("IT'S BUSINESS TIME! No seriously, where's the corporate balance"
         " sheet?"),
        "It's nice out, you look nice, why not get freaky?",
    ],
    'sleet': [
        "WHAAAAAAA"
    ],
    'fog': [
        "Is someone talking? I can't see anything.",
        "It's froggy out. Wait hahahahahahaha I made an oops",
    ],
    'partly-cloudy-day': [
        "SHIT JUST GOT REAL",
        "START YOUR FUCKING DAY ALREADY",
        "Good enough to eat outside.",
        "I'm getting naked now, just in case it gets nicer",
        "I'd put my bikini on, but I forgot the bottom... and the top.",
    ],
    'partly-cloudy-night': [
        "I'm going to bed. You do what you want.",
        "PANTS OFF. Jokes on you, they were never on.",
        "I'm getting a hooker right now.",
    ],
    'wind': [
        "This pollen count is FUCKING KILLING ME",
        "I'd hit the waves, but they hit back",
        "FUCK! MY ICE CREAM CONE BLEW AWAY",
        "Listen! This is important! It's windy! That is all!",
    ],
    'hail': [
        "What's so nice? This shit hurts!",
        "FUCKIN' OW!",
    ],
}


def generate_text(temp, condition='default', region='us'):
    if region != 'us':
        temp = 9.0 / 5.0 * temp + 32
    if temp <= 49:
        text = "IT'S FUCKING COLD!"
        group = cold
    elif 50 <= temp <= 63:
        text = "IT'S FUCKING.... ALRIGHT."
        group = alright
    elif 64 <= temp <= 79:
        text = "IT'S FUCKING NICE!"
        group = nice
    elif 80 <= temp <= 97:
        text = "IT'S FUCKING HOT!"
        group = hot
    elif temp >= 98:
        text = "IT'S FUCKING... ARE YOU OK?"
        group = hot

    if random.randint(1, 2) == 1:
        subtext = random.choice(group['default'])
    else:
        subtext = random.choice(group[condition])

    return str('%s (%s)' % (text, subtext))
