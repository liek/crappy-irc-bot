# encoding: utf-8
"""
fuckingweather.py - cib 'The Fucking Weather'  Module
Copyright 2012, betrayed/syrius
"""

from . import fio
import pickle
from . import fuckingtext

fw_apikey = None

indent = "  "

fuckingweather_file = "fuckingweather.p"
fw_default = {}

plugin = None


def init(p, bot):
    global plugin, fw_apikey, fw_geocodekey, fw_default, fuckingweather_file
    plugin = p
    plugin.bind_public('!fw', handle_fw)
    irc = bot

    try:
        fw_apikey = irc.cfg.get('fw', 'apikey')
    except:
        pass

    try:
        fw_geocodekey = irc.cfg.get('fw', 'geocodekey')
    except:
        pass

    try:
        fw_default = dict(pickle.load(open(fuckingweather_file, "rb")))
    except:
        pass


def handle_fw(irc, sender, channel, args):
    global fw_default, fuckingweather_file, plugin
    region = 'us'
    if len(args) > 1 and args[1] == '-d':
        umask = sender.get_usermask()
        if len(args) > 2:
            fw_default[umask] = ' '.join(args[2:])
            irc.notice(sender.nick,
                       "Set your default to %s" % (fw_default[umask]))
            try:
                pickle.dump(fw_default, open(fuckingweather_file, "wb"))
            except:
                pass
            return
        else:
            if umask in fw_default:
                irc.privmsg(channel,
                            "%s: Your default is %s" %
                            (sender.nick, fw_default[umask]))
            else:
                irc.privmsg(channel,
                            "%s: You don't have a fucking default" %
                            (sender.nick))
            return
    elif len(args) < 2:
        umask = sender.get_usermask()
        if umask in fw_default:
            location = fw_default[umask]
        else:
            irc.privmsg(channel,
                        "%s: You don't have a fucking default" %
                        (sender.nick))
            return
    elif len(args) > 1 and args[1] == '-c':
        region = 'si'
        location = ' '.join(args[2:])
    else:
        location = ' '.join(args[1:])

    f_cast = None
    f_hi = None
    temp = None
    city = None
    alert = None
    try:
        forecast = fio.Forecastio(fw_apikey)
        look = fio.Lookup(location, fw_geocodekey)
        if 'error' in look:
            plugin.logger.exception("error fetching fio.Lookup: {}".format(look))
            irc.privmsg(channel, "Error: %s" % (look['data']))
            return
        result = forecast.get_weather(look['latitude'], look['longitude'],
                                      region=region)
        if result['error'] is False:
            city = look['city']
            current = forecast.get_Currently(offset=forecast.get_offset())
            daily = forecast.get_Daily(offset=forecast.get_offset())
            temp = int(round(current.temperature))
            hum = int(round(current.humidity * 100))
            alert = forecast.get_Alert()
            f_hi = current.apparentTemperature
            f_cast = "Forecast for %s: %s High %s/Low %s/Hum %s%%" % (
                daily.data[0].time.strftime('%a'),
                current,
                int(round(daily.data[0].temperatureMax)),
                int(round(daily.data[0].temperatureMin)),
                hum)
        else:
            plugin.logger.exception("weather error: {}".format(result))
            irc.privmsg(channel, "error: %s" % result)
            return

    except Exception as e:
        irc.privmsg(channel, "Error retrieving Fucking weather data: %s" %
                    e)
        return
    irc.privmsg(channel, "Weather for %s" % (city))
    irc.privmsg(channel, "%s%s°(%s°)" % (indent, temp, int(round(f_hi))))
    fucking = fuckingtext.generate_text(temp, current.icon, region)
    if f_cast:
        irc.privmsg(channel, "%s%s %s" % (indent, fucking, f_cast))
    else:
        irc.privmsg(channel, "%s%s" % (indent, fucking))
    if alert is not None:
        irc.privmsg(channel,
                    "Alert: %s. More info: %s" %
                    (alert[0].title, alert[0].uri))
