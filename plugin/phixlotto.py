import datetime
import pickle
import json
import random
import operator
import fnmatch

import urllib.request as UL
from urllib.parse import urlencode


plugin = None

phixlotto_file = "phixlottostats.p"
kstats_file = "kstats.p"
# example: ['#foo', '#bar']
phixlotto_channels = []
# store everything lower case
# hosts format: ['user@hostmask']
# alias format: ['foo', 'bar']
phixlotto_blacklist = {}

phixlottoupdate_url = 'http://www.liek.net/cgi-bin/mnews/plstats.cgi'
phixlottoupdate_login = ""
phixlottoupdate_password = ""


def init(p, bot):
    global plugin, phixlotto_timer, phixlotto_channels, phixlotto_blacklist
    global phixlottoupdate_login, phixlottoupdate_password

    plugin = p

    plugin.subscribe('kick', kick)
    plugin.bind_public('!plstats', plstats)
    plugin.bind_public('!kstats', kstats)
    irc = bot

    try:
        phixlotto_channels = irc.cfg.get('phixlotto', 'channels').split()
    except:
        pass
    try:
        phixlotto_blacklist = json.loads(irc.cfg.get('phixlotto', 'blacklist'))
    except:
        pass

    try:
        phixlottoupdate_login = irc.cfg.get('phixlotto', 'update_login')
    except:
        pass
    try:
        phixlottoupdate_password = irc.cfg.get('phixlotto', 'update_password')
    except:
        pass

    create_kick_timer(irc)


def plstats(irc, sender, channel, args):
    try:
        user_stats = dict(pickle.load(open(phixlotto_file, "rb")))
    except:
        irc.privmsg(channel, "No kick records found.")
        return

    if len(args) > 1 and args[1] == 'all':
        sorted_kicks = sorted(iter(list(user_stats.items())),
                              key=operator.itemgetter(1), reverse=True)
        irc.privmsg(channel, "Top Phix Lotto \"Winners\"")

        for i in range(0, min(10, len(sorted_kicks))):
            (nick, count) = sorted_kicks[i]

            irc.privmsg(channel, "%d) \002%s\002: %d" % (i, nick, count))
    else:
        nick_check = irc.irclower(args[1]) \
            if len(args) > 1 else irc.irclower(sender.nick)

        kick_count = user_stats.get(nick_check, 0)
        irc.privmsg(channel, "Kick count for %s: %d" %
                    (nick_check, kick_count))


def kstats(irc, sender, channel, args):
    try:
        kstats = dict(pickle.load(open(kstats_file, "rb")))
    except:
        irc.privmsg(channel, "Unable to load kick stats")
        return

    sorted_kstats = sorted(iter(kstats.items()),
                           key=operator.itemgetter(1), reverse=True)
    irc.privmsg(channel, "The Big Three Stats:")
    for u in sorted_kstats:
        (nick, count) = u
        irc.privmsg(channel, "\002%s\002: %d" % (nick, count))


def kick(irc, sender, channel, nick, message):
    global phixlotto_blacklist, kstats_file

    user = irc.chmgr.get(channel).get_user(nick)

    knick = irc.irclower(nick)
    try:
        kstats = dict(pickle.load(open(kstats_file, "rb")))
    except:
        kstats = dict((i, 0) for i in phixlotto_blacklist.keys())

    target = ""

    if knick in phixlotto_blacklist:
        target = knick
    else:
        for t in phixlotto_blacklist:
            for host in phixlotto_blacklist[t]['hosts']:
                if fnmatch.fnmatch("%s@%s" %
                                   (user.username.lower(),
                                    user.host.lower()),
                                   host):
                    target = t
                    break

            if not target and knick in phixlotto_blacklist[t]['aliases']:
                target = t

    if target:
        if target in kstats:
            kstats[target] += 1
        else:
            kstats[target] = 1
        try:
            pickle.dump(kstats, open(kstats_file, "wb"))
        except:
            pass
        try:
            pl_updateweb(kstats, 'kstats_data')
        except:
            pass


def get_user(irc, users):
    global phixlotto_blacklist

    if not users:
        return None

    blacklist_size = len(phixlotto_blacklist)

    n = random.randint(0, blacklist_size)

    # choose from pre-defined nick
    if n < blacklist_size:
        tard = irc.irclower(list(phixlotto_blacklist.keys())[n])

        #if tard in map(lambda x: x.lower(), users.keys()):
        if tard in users.keys():
            return tard

        hosts = phixlotto_blacklist[tard]['hosts']

        for nick, user in users.items():
            for host in hosts:
                if fnmatch.fnmatch("%s@%s" % (user.username.lower(),
                                              user.host.lower()),
                                   host):
                    return nick

        # this user isn't on. get another
        return get_user(irc, users)

    # get random channel user
    return list(users.keys())[random.randint(0, len(users) - 1)]


def kick_phix(irc):
    global phixlotto_timer
    global phixlotto_channels

    for channel in phixlotto_channels:
        chan = irc.chmgr.get(channel)

        if not chan or not chan.users:
            continue

        channel_users = chan.users
        # don't kick ourselves to help prevent kick lotto outages.
        try:
            del channel_users[irc.irclower(irc.nickname)]
        except KeyError:
            plugin.logger.error(
                "[phixlotto] couldn't delete myself from nick list")

        knick = get_user(irc, channel_users)

        # phix gets skipped on wednesdays
        if datetime.datetime.today().weekday() == 2:
            while knick == "phix" \
                    or knick in phixlotto_blacklist['phix']['aliases']:
                knick = get_user(irc, channel_users)

        try:
            user_stats = dict(pickle.load(open(phixlotto_file, "rb")))
        except:
            user_stats = {}

        main_nick = knick

        # find alias for stats purposes
        for nick in phixlotto_blacklist:
            if knick in phixlotto_blacklist[nick]['aliases']:
                main_nick = irc.irclower(nick)
                break

        kick_count = user_stats.get(main_nick, 0) + 1
        user_stats[main_nick] = kick_count

        irc.kick(channel,
                 knick,
                 "You are the lucky winner of the phix lotto #%d" %
                 sum(user_stats.values()))

        try:
            pickle.dump(user_stats, open(phixlotto_file, "wb"))
        except:
            pass

        try:
            pl_updateweb(user_stats, 'plstats_data')
        except Exception as e:
            plugin.logger.error("web update error: %s" % (e))

    # run it again on the hour
    return True


def create_kick_timer(irc):
    now = datetime.datetime.today()
    kick_time = now + datetime.timedelta(hours=1, minutes=-now.minute)
    plugin.schedule_periodic(irc, when=kick_time, function=kick_phix,
                             interval=datetime.timedelta(hours=1))


def pl_updateweb(stats, key):
    global phixlottoupdate_login, phixlottoupdate_password

    json_data = json.dumps(stats)
    q = urlencode([('login', phixlottoupdate_login),
                   ('password', phixlottoupdate_password),
                   (key, json_data)]).encode('utf-8')

    req = UL.Request(phixlottoupdate_url, q)
    req.add_header("Content-type", "application/x-www-form-urlencoded")
    UL.urlopen(req, timeout=15)
