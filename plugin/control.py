import sys

plugin = None

authorized_clients = []
hosts = []
password = None
cmd_char = '~'
cmd_table = {}
info_table = {}

help_table = {
    'auth': ['<password> [command...]'],
    'load': ['<plugin1> [plugin2]...'],
    'unload': ['<plugin1> [plugin2]...'],
    'reload': [''],
    'die': [''],
    'say': ['<target> <message>'],
    'act': ['<target> <message>'],
    'notice': ['<target> <message>'],
    'join': ['<channel1> [channel2...'],
    'part': ['<channel1> [channel2...'],
    'nick': ['<nickname>'],
    'mode': ['[channel] <+-modes> [args]'],
    'help': ['[command]'],
    'kick': ['<channel> <target> [message]'],
    'plugins': [''],
    'add': ['<plugin>'],
    'remove': ['<plugin>'],
}


def init(p, bot):
    global plugin, cmd_table, info_table, hosts, password, cmd_char

    irc = bot
    plugin = p

    plugin.subscribe('part', handle_part)
    plugin.subscribe('quit', handle_quit)
    plugin.subscribe('nick', handle_nick)
    plugin.subscribe('kick', handle_kick)
    plugin.subscribe('disconnect', handle_disconnect)

    try:
        hosts = list(irc.cfg.get('control', 'hosts').split())
    except:
        pass
    try:
        password = irc.cfg.get('control', 'password')
    except:
        pass
    try:
        cmd_char = irc.cfg.get('control', 'command_char')
    except:
        pass

    cmd_table['auth'] = (command_auth, True, 1)
    cmd_table['load'] = (command_load, True)
    cmd_table['unload'] = (command_unload, True)
    cmd_table['reload'] = (command_reload, True)
    cmd_table['die'] = (command_die, False)
    cmd_table['say'] = (command_say, False)
    cmd_table['act'] = (command_act, False)
    cmd_table['notice'] = (command_notice, False)
    cmd_table['join'] = (command_join, True)
    cmd_table['part'] = (command_part, True)
    cmd_table['nick'] = (command_nick, True)
    cmd_table['mode'] = (command_mode, True)
    cmd_table['kick'] = (command_kick, True, 2)
    cmd_table['add'] = (command_add, True)
    cmd_table['rem'] = (command_rem, True)
    cmd_table['remove'] = (command_rem, True)
    info_table['help'] = (command_help, True)
    info_table['plugins'] = (command_plugins, True)

    for t in cmd_table:
        plugin.bind_private(cmd_char + t, *cmd_table[t])
    for t in info_table:
        plugin.bind_private(cmd_char + t, *info_table[t])

    plugin.logger.info("control.py by Matt@#liek loaded.")


def handle_part(irc, sender, channel, message):
    global authorized_clients
    users = []

    if sender.nick == irc.nickname:
        chan = irc.chmgr.get(channel)
        users = chan.names()
    else:
        users = [sender.nick]

    check_users(irc, channel, users)


def handle_kick(irc, sender, channel, nick, message):
    global authorized_clients

    users = []

    if nick == irc.nickname:
        chan = irc.chmgr.get(channel)
        users = chan.names()
    else:
        users = [nick]

    check_users(irc, channel, users)


def check_users(irc, leaving_channel, users):
    # Used to make sure we're still on a common channel with
    # authorized users

    global authorized_clients

    for u in users:
        if u in authorized_clients:
            channels = irc.chmgr.channels
            found = False
            for ch in channels:
                if ch != leaving_channel:
                    if irc.chmgr.get(ch).get_user(u):
                        found = True
                        break
            if not found:
                irc.notice(u, "No longer authorized, I can't see you.")
                authorized_clients.remove(u)


def handle_quit(irc, sender, message):
    global authorized_clients

    if sender.nick in authorized_clients:
        authorized_clients.remove(sender.nick)


def handle_nick(irc, sender, newnick):
    global authorized_clients

    if sender.nick in authorized_clients:
        authorized_clients.remove(sender.nick)
        authorized_clients.append(newnick)


def handle_disconnect(irc, sender, **kwargs):
    global authorized_clients
    del authorized_clients[:]


def is_authenticated(sender):
    global hosts, authorized_clients
    if sender.host in hosts:
        return True
    elif sender.nick in authorized_clients:
        return True
    return False


def _auth_check(func):
    def handler(irc, sender, args, authorized=False):
        global hosts, authorized_clients
        if authorized \
            or sender.host in hosts \
                or sender.nick in authorized_clients:
            return func(irc, sender, args)
    return handler

# Commands go here


def command_auth(irc, sender, args):
    global authorized_clients, hosts

    if len(args) < 2:
        send_helpmsg(irc, sender.nick, 'auth')
        return

    if args[1] != password:
        irc.notice(sender.nick, "Uh Uh Uh. You didn't say the magic word.")
        return

    if len(args) > 2:
        # Exec'ing a command - useful if not on any common channels
        command, cmd_args = args[2].partition(' ')[::2]
        if command not in cmd_table:
            return
        func, split = cmd_table[command][0:2]
        if not split:
            count = 0
        elif len(cmd_table[command]) > 2:
            count = cmd_table[command][2]
        else:
            count = -1
        return func(irc, sender,
                    [cmd_char + command] + cmd_args.split(None, count), True)
    else:
        # Check if we're not in any common channels
        found = False
        for c in irc.chmgr.channels:
            if irc.chmgr.get(c).get_user(sender.nick):
                found = True
                break
        if found:
            authorized_clients.append(sender.nick)
            irc.notice(sender.nick, "Welcome, I'll do your bidding now.")
        else:
            irc.notice(sender.nick, "Come closer, I can't see you.")


@_auth_check
def command_load(irc, sender, args):
    plugins = args[1:]
    if len(plugins) > 0:
        irc.notice(sender.nick, "Loading Plugin%s: %s" % (
                   's' if len(plugins) > 1 else '', ', '.join(plugins)))
        for plugin in plugins:
            irc.load_plugin(plugin)
    else:
        send_helpmsg(irc, sender.nick, 'load')
        return


@_auth_check
def command_unload(irc, sender, args):
    plugins = args[1:]
    if len(args) > 1:
        irc.notice(sender.nick, "Unloading Plugin%s: %s" % (
                   's' if len(plugins) > 1 else '', ', '.join(plugins)))
        for plugin in plugins:
            irc.unload_plugin(plugin)
    else:
        send_helpmsg(irc, sender.nick, 'unload')
        return


@_auth_check
def command_reload(irc, sender, args):
    irc.notice(sender.nick, "Reloading all plugins")
    for plugin in list(irc.pmgr._plugins.keys()):
        irc.unload_plugin(plugin)
        irc.load_plugin(plugin)


@_auth_check
def command_die(irc, sender, args):
    quit_msg = 'Aieeeeeeeeeeeee'
    if len(args) > 1 and len(args[1]) > 0:
        quit_msg = args[1]
    irc.notice(sender.nick, "Terminating: %s" % (quit_msg))
    irc.terminate(quit_msg)


@_auth_check
def command_say(irc, sender, args):
    try:
        target, message = args[1].split(None, 1)
    except:
        send_helpmsg(irc, sender.nick, 'say')
        return
    irc.privmsg(target, message)


@_auth_check
def command_act(irc, sender, args):
    try:
        target, message = args[1].split(None, 1)
    except:
        send_helpmsg(irc, sender.nick, 'act')
        return
    irc.action(target, message)


@_auth_check
def command_notice(irc, sender, args):
    try:
        target, message = args[1].split(None, 1)
    except:
        send_helpmsg(irc, sender.nick, 'notice')
        return
    irc.notice(target, message)


@_auth_check
def command_join(irc, sender, args):
    if len(args) < 2:
        send_helpmsg(irc, sender.nick, 'join')
        return
    channels = args[1:]
    irc.notice(sender.nick, 'Joining %s' % (','.join(channels)))
    irc.joinc(','.join(channels))


@_auth_check
def command_part(irc, sender, args):
    if len(args) < 2:
        send_helpmsg(irc, sender.nick, 'part')
        return
    channels = args[1:]
    irc.notice(sender.nick, 'Leaving %s' % (','.join(channels)))
    irc.part(','.join(channels))


@_auth_check
def command_nick(irc, sender, args):
    if not args or not args[1]:
        send_helpmsg(irc, sender.nick, 'nick')
        return
    new_nick = args[1]
    irc.notice(sender.nick, 'Changing nickname to %s' % (new_nick))
    irc.nick(new_nick)


@_auth_check
def command_mode(irc, sender, args):
    modes = args[1:]
    if not modes:
        send_helpmsg(irc, sender.nick, 'mode')
        return
    if modes[0][0] == '+' or modes[0][0] == '-':
        # Modeless channels, right? (ircu...)
        irc.usermode(irc.nickname, modes[0])
        irc.notice(sender.nick, 'Setting mode: %s' % (modes[0]))
    elif len(modes) > 1:
        irc.notice(sender.nick, 'Setting mode on %s: %s' % (
                   modes[0], ' '.join(modes[1:])))
        irc.mode(modes[0], modes[1], ' '.join(modes[2:]))
    else:
        send_helpmsg(irc, sender.nick, 'mode')


@_auth_check
def command_kick(irc, sender, args):
    if len(args) < 3:
        send_helpmsg(irc, sender.nick, 'kick')
        return
    irc.notice(sender.nick, "Kicking %s from %s (%s)" % (
               args[2], args[1],
               args[3] if len(args) > 3 else ""))
    irc.kick(args[1], args[2],
             args[3] if len(args) > 3 else None)


def command_help(irc, sender, args):
    if len(args) > 1:
        command = args[1].lower()
        if command in help_table:
            send_helpmsg(irc, sender.nick, command, True)
            return
    cmds = list(help_table.keys())
    cmds.sort()
    irc.notice(sender.nick, "Available commands: %s" % (", ".join(cmds)))


@_auth_check
def command_add(irc, sender, args):
    if len(args) < 2:
        send_helpmsg(irc, sender.nick, 'add')
        return
    pname = 'plugin.' + args[1]
    if pname not in sys.modules:
        command_load(sender, irc, args[1:2])
    try:
        irc.cfg.get('plugins', args[1])
    except:
        irc.cfg.set('plugins', args[1])
        irc.notice(sender.nick, 'Added plugin: %s' % (args[1]))
        try:
            irc.cfg.write(open(irc.cfg_file, 'w'))
        except:
            irc.notice(sender.nick, 'Error saving configuration file!')


@_auth_check
def command_rem(irc, sender, args):
    if len(args) < 2:
        send_helpmsg(irc, sender.nick, 'remove')
        return
    pname = 'plugin.' + args[1]
    if pname in sys.modules:
        command_unload(sender, irc, args[1:2])
    try:
        irc.cfg.get('plugins', args[1])
    except:
        irc.notice(sender.nick, "That plugin isn't in my autoload list.")
        return
    try:
        irc.cfg.remove_option('plugins', args[1])
        irc.notice(sender.nick, "Removed plugin: %s" % (args[1]))
    except:
        irc.notice(sender.nick, "Error removing plugin from autoload list.")
        return
    try:
        irc.cfg.write(open(irc.cfg_file, 'w'))
    except:
        irc.notice(sender.nick, 'Error saving configuration file!')
        return


def command_plugins(irc, sender, args):
    my_list = []
    for plugin in list(irc.pmgr._plugins.keys()):
        p = plugin.replace('plugin.', '')
        my_list.append(p)
    irc.notice(sender.nick, "Loaded Plugins: %s" % (', '.join(my_list)))


def send_helpmsg(irc, target, command, full=False):
    global help_table
    if not irc or not target:
        return
    if command in help_table:
        irc.notice(target, "\002%s%s\002: %s" % (
                   cmd_char, command, help_table[command][0]))
