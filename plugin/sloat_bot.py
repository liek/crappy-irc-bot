import threading
import random
import time

irc = None


def init(bot):
    global irc
    irc = bot

    sb = SloatBot(irc=bot)
    sb.start()


class SloatBot(threading.Thread):
    def __init__(self, group=None, target=None, name=None, *args, **kwargs):
        threading.Thread.__init__(self, group, target, name, args, kwargs)
        self.irc = kwargs['irc']
        self.daemon = True

        self.starts = ['Jesus christ', 'holy shit,', 'fuckin\'', 'kaboofa,',
                       'betrayed,']

    def run(self):
        while True:
            time.sleep(90)

            if not self.irc.connected:
                break

            hmm = random.randint(0, 10)
            if hmm == 1:
                self.do_something()
            elif hmm == 4:
                self.say_crap()

    def do_something(self):
        say = []
        if random.randint(0, 4) == 1:
            say.append(random.choice(self.starts))

        if random.randint(0, 4) <= 1:
            thing, end = self.i_hate()
        else:
            thing, end = self.i_liek()

        say.append(thing)
        say.append(end)

        self.irc.privmsg(random.choice(list(self.irc.chmgr.channels.keys())),
                         " ".join(say))

    def i_hate(self):
        endings = ['can eat a bag.', 'is unbelievably terrible.',
                   '...i just can\'t believe how much it sucks.',
                   'fucking sucks dammit.']

        stuff = ['godaddy', 'IRC', 'this client', 'mIRC', 'work']

        return (random.choice(stuff), random.choice(endings))

    def i_liek(self):
        endings = ['...awesome.', 'fucking rules.', '-- fuck yeah!',
                   'is the best thing on this earth.',
                   'is better than sex times 30.']

        stuff = ['this donut', 'tea', 'zombies', 'sex', 'vodka']

        return (random.choice(stuff), random.choice(endings))

    def say_crap(self):
        size = ['', '2nd', '3rd', '4th', '5th']
        say = "That was the %s biggest dump I've ever taken." % \
            random.choice(size)
        self.irc.privmsg(random.choice(list(self.irc.chmgr.channels.keys())),
                         say)
