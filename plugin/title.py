import re
import fnmatch
import util

import urllib.request as UL
from urllib.error import URLError
from html.parser import HTMLParser

plugin = None

title = re.compile(r'(?ims)<title[^>]*>(.*?)</title\s*>')
match_url = re.compile(r'https?://\S+')

url_blacklist = ["http*://*youtube.com/*",
                 "http*://youtu.be/*",
                 "http*://imgur.com/*",
                 "http*://www.imgur.com/*",
                 "http*://i.imgur.com/*",
                 "http*://twitter.com/*",
                 ]


def init(p, bot):
    global plugin, url_blacklist
    plugin = p
    plugin.subscribe('pubmsg', pubmsg)
    irc = bot
    try:
        url_blacklist = [_[0] for _ in irc.cfg.items('url_blacklist')]
    except:
        pass


def pubmsg(irc, sender, channel, message):
    mtitle = ""
    hp = HTMLParser()

    m = match_url.search(message)

    if m:
        link = m.group()
        for mask in url_blacklist:
            if fnmatch.fnmatch(link, mask):
                return

        try:
            request = UL.build_opener()
            request.addheaders = [('User-agent', 'Liek/1.0')]
            infile = request.open(link, timeout=5)

            ctype = infile.headers.get('Content-Type', "")
            if not ctype.startswith('text/html'):
                return

            html = util.decode(infile.read())
            infile.close()

            topic = title.search(html)
            if topic:
                mtitle = topic.group(1)
                mtitle = mtitle.strip()
                mtitle = hp.unescape(mtitle)
                mtitle = mtitle.replace("\t", " ")
                mtitle = mtitle.replace("\r", " ")
                mtitle = mtitle.replace("\n", " ")
                mtitle = mtitle.replace("\x03", "")
                if len(mtitle) > 175:
                    mtitle = mtitle[:175] + "..."
        except URLError as e:
            mtitle = "Error: %s" % (e)
        except:
            mtitle = "Could not connect to that site"

        # don't print the link back out... chaos ensues with more than one bot
        irc.privmsg(channel, "<Title> %s" % (mtitle))
