#slightly less simmple auto re-join plugin

plugin = None
channels = []


def init(p, bot):
    global plugin, channels
    plugin = p
    plugin.subscribe('kick', handle_kick)
    plugin.subscribe('rpl_welcome', handle_001)
    chans = bot.cfg.get('main', 'channels').split()

    for i in chans:
        chan = ""
        key = ""
        if ':' in i:
            chan, key = i.split(':')
        else:
            chan = i
        channels.append((chan, key))


def handle_kick(irc, sender, channel, nick, message):
    if nick == irc.nickname:
        irc.joinc(channel)


def handle_001(irc, sender, args):
    global plugin
    plugin.schedule_periodic(irc, interval=10.0, function=check_channels)


def check_channels(irc):
    global channels
    if not irc.connected:
        return False

    for c in channels:
        chname, key = c
        chan = irc.chmgr.get(chname)
        if not chan.channel:
            # XXX: We should probably change this.
            irc.joinc(chname, key)

    return True
