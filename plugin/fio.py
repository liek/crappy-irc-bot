import datetime
import time
import json
import requests

from urllib.parse import quote_plus

class Forecastio():
    def __init__(self, apikey):
        self.apikey = apikey
        self.latitude = None
        self.longitutde = None
        self.furl = None
        self.json = None

    def get_weather(self, latitude, longitude, time=None, region="auto"):
        self.latitude = latitude
        self.longitude = longitude
        self.time = time
        self.offset = None
        if self.time is None:
            self.furl = 'https://api.darksky.net/forecast/' + \
                str(self.apikey) + '/' + str(self.latitude) + ',' + \
                str(self.longitude) + '?units=' + region
        else:
            self.furl = 'https://api.darksky.net/forecast/' + \
                str(self.apikey) + '/' + str(self.latitude) + ',' + \
                str(self.longitude) + ',' + str(self.time) + '?units=' + \
                region
        try:
            response = requests.get(self.furl, verify=False, headers={'Accept-encoding': 'gzip'}, timeout=5)
            self.json = response.json()
            return {'error': False, 'data': self.json}
        except requests.exceptions.HTTPError as e:
            return {'error': True, 'data': e.reason}
        except Exception as e:
            return {'error': True, 'data': e}

    def get_offset(self):
        loffset = time.timezone if (time.localtime().tm_isdst == 0) \
            else time.altzone
        loffset1 = (loffset / 60 / 60 * -1) - self.json['offset']
        return loffset1

    def get_Heatindex(self, temp, hum):
        try:
            if (hum < 40 or temp < 80):
                return temp
            index = (-42.379 + 2.04901523 * temp) + (10.14333127 * hum) + \
                    (-0.22475541 * temp * hum) + \
                    ((-6.83783 * pow(10.0, -3.0)) * pow(temp, 2.0)) + \
                    ((-5.481717 * pow(10.0, -2.0)) * pow(hum, 2.0)) + \
                    ((1.22874 * pow(10.0, -3.0)) * pow(temp, 2.0) * hum) + \
                    ((8.5282 * pow(10.0, -4.0)) * temp * pow(hum, 2.0)) + \
                    ((-1.99 * pow(10.0, -6.0)) * pow(temp, 2.0) * pow(hum, 2.0)
                     )
            return index
        except:
            return None

    def get_Currently(self, offset=0):
        try:
            return dpoint(self.json['currently'], offset)
        except:
            return dpoint()

    def get_Daily(self, offset=0):
        try:
            return dblock(self.json['daily'], offset)
        except:
            return dblock()

    def get_Minutely(self, offset=0):
        try:
            return dblock(self.json['minutely'], offset)
        except:
            return dblock()

    def get_Hourly(self, offset=0):
        try:
            return dblock(self.json['hourly'], offset)
        except:
            return dblock()

    def get_Alert(self):
        self.Alerts = []
        try:
            for data in self.json['alerts']:
                self.Alerts.append(apoint(data))
            return self.Alerts
        except:
            return None


class apoint():
    def __init__(self, data=None):
        try:
            self.title = data['title']
        except:
            self.title = None
        try:
            self.uri = data['uri']
        except:
            self.uri = None
        try:
            self.expires = data['expires']
        except:
            self.expires = None
        try:
            self.description = data['description']
        except:
            self.description = None

    def __str__(self):
        return str(self).encode('utf-8')


class dblock():
    def __init__(self, data=None, offset=0):
        try:
            self.summary = data['summary']
        except:
            self.summary = None
        try:
            self.icon = data['icon']
        except:
            self.icon = None
        self.data = []
        if data is not None:
            for point in data['data']:
                self.data.append(dpoint(point, offset))

    def __str__(self):
        return str(self).encode('utf-8')


class dpoint():
    def __init__(self, data=None, offset=0):
        try:
            self.summary = data['summary']
        except:
            self.summary = None
        try:
            self.icon = data['icon']
        except:
            self.icon = None
        try:
            self.time = (datetime.datetime.fromtimestamp(int(data['time']))) \
                - datetime.timedelta(hours=offset)
        except:
            self.time = None
        try:
            self.sunriseTime = datetime.datetime.fromtimestamp(
                int(data['sunriseTime']))
        except:
            self.sunriseTime = None
        try:
            self.sunsetTime = datetime.datetime.fromtimestamp(
                int(data['sunsetTime']))
        except:
            self.sunsetTime = None
        try:
            self.precipIntensity = data['precipIntensity']
        except:
            self.precipIntensity = None
        try:
            self.precipIntensityMax = data['precipIntensityMax']
        except:
            self.precipIntensityMax = None
        try:
            self.precipIntensityMaxTime = data['precipIntensityMaxTime']
        except:
            self.precipIntensityMaxTime = None
        try:
            self.precipProbablility = data['precipProbablility']
        except:
            self.precipProbablility = None
        try:
            self.precipType = data['precipType']
        except:
            self.precipType = None
        try:
            self.precipAccumulation = data['precipAccumulation']
        except:
            self.precipAccumulation = None
        try:
            self.temperature = data['temperature']
        except:
            self.temperature = None
        try:
            self.temperatureMin = data['temperatureMin']
        except:
            self.temperatureMin = None
        try:
            self.temperatureMinTime = data['temperatureMinTime']
        except:
            self.temperatureMinTime = None
        try:
            self.temperatureMax = data['temperatureMax']
        except:
            self.temperatureMax = None
        try:
            self.temperatureMaxTime = datetime.datetime.fromtimestamp(
                int(data['temperatureMaxTime']))
        except:
            self.temperatureMaxTime = None
        try:
            self.dewPoint = data['dewPoint']
        except:
            self.dewPoint = None
        try:
            self.windspeed = data['windSpeed']
        except:
            self.windspeed = None
        try:
            self.windbaring = data['windBearing']
        except:
            self.windbaring = None
        try:
            self.cloudcover = data['cloudCover']
        except:
            self.cloudcover = None
        try:
            self.humidity = data['humidity']
        except:
            self.humidity = None
        try:
            self.pressure = data['pressure']
        except:
            self.pressure = None
        try:
            self.visbility = data['visbility']
        except:
            self.visbility = None
        try:
            self.ozone = data['ozone']
        except:
            self.ozone = None
        try:
            self.apparentTemperature = data['apparentTemperature']
        except:
            self.apparentTemperature = None

    def __str__(self):
        return "%s" % (self.summary)


def Lookup(lookup=None, apikey=None):
    if lookup is None:
        lookup = '60103'
    lookup = lookup.encode('utf-8')
    geolookup = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + \
        quote_plus(lookup) + '&sensor=false&key=' + apikey

    try:
        res = requests.get(geolookup, verify=False, timeout=5)
        georesponsedata = res.json()['results']

        if georesponsedata:
            georesponsedata = georesponsedata[0]
        else:
            return {'error': True, 'data': 'Location not found'}
    except requests.exceptions.HTTPError as e:
        return {'error': True, 'data': e.reason}
    except IndexError as e:
        return {'error': True, 'data': ans['error_message']}
    except Exception as e:
        return {'error': True, 'data': e}
    georesponse = {}
    try:
        georesponse['latitude'] = str(
            georesponsedata['geometry']['location']['lat'])
        georesponse['longitude'] = str(
            georesponsedata['geometry']['location']['lng'])
        georesponse['city'] = georesponsedata['formatted_address']
        return georesponse
    except AttributeError as e:
        return {'error': True, 'data': e}
