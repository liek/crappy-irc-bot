plugin = None

chanlogs = {}
# store user@host in blacklist - this will trigger for a user even if
# they are op'd
blacklist = []
repeatmax = 5


def init(p, bot):
    global plugin
    plugin = p
    plugin.subscribe('pubmsg', pubmsg)
    plugin.subscribe('notice', notice)


def getlog(channel):
    global chanlogs

    try:
        lastlog = chanlogs[channel]
    except KeyError:
        chanlogs[channel] = []
        lastlog = chanlogs[channel]

    return lastlog


def addtolog(channel, host):
    lastlog = getlog(channel)

    if len(lastlog) == repeatmax:
        shiftlog(lastlog)

    lastlog.append(host)


def shiftlog(lastlog):
    logcount = len(lastlog)

    if logcount < repeatmax:
        return

    for i in range(1, logcount):
        lastlog[i - 1] = lastlog[i]

    lastlog.pop()


def checkmutenick(channel):
    lastlog = getlog(channel)

    logcount = len(lastlog)

    if not logcount or logcount < repeatmax:
        return

    return logcount == lastlog.count(lastlog[0])


def getuser(sender):
    return "%s@%s" % (sender.username, sender.host)


def pubmsg(irc, sender, channel, message):
    global chanlogs
    global blacklist

    chan = irc.chmgr.get(channel)

    if not channel or channel[:1] != '#':
        return

    user = getuser(sender)

    addtolog(channel, user)

    if chan.isop(sender.nick) and not blacklist.count(user):
        return

    if checkmutenick(channel):
        people = chan.names()

        voices = []

        for person in people:
            if person == sender.nick:
                continue

            if not chan.isop(person) and not chan.isvoice(person):
                voices.append(person)

        if voices:
            irc.mode(channel, "+v" * len(voices), " ".join(voices))

        if chan.isvoice(sender.nick):
            irc.mode(channel, "-v", sender.nick)

        if chan.isop(sender.nick):
            irc.mode(channel, "-o", sender.nick)

        log = getlog(channel)
        del log[:]

        irc.mode(channel, "+m")
        plugin.schedule(irc, when=5, function=irc.mode, argv=[channel, "-m"])


def notice(irc, sender, message):
    global blacklist

    if message == "chanlogs" and sender.nick == "syrius":
        irc.notice(sender.nick, repr(chanlogs))

    if message == "blacklist" and sender.nick == "syrius":
        irc.notice(sender.nick, repr(blacklist))

    cmds = message.split()

    if cmds[0][1:] == "blist" and sender.nick == "syrius":
        host = cmds[1]

        if not host:
            return

        action = cmds[0][:1]

        if action == '+':
            if not blacklist.count(host):
                blacklist.append(host)
                irc.notice(sender.nick, "Added %s to blacklist" % host)
            else:
                irc.notice(sender.nick, "%s is already in blacklist" % host)
        elif action == '-':
            if not blacklist.count(host):
                irc.notice(sender.nick, "%s does not exist in blacklist" %
                           host)
            else:
                blacklist.remove(host)
                irc.notice(sender.nick, "%s removed from blacklist" % host)
