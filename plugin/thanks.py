import re

plugin = None
m_thanks = re.compile(r'\bthanks ([^\s]+)', re.I)


def init(p, bot):
    global plugin
    plugin = p
    plugin.subscribe('pubmsg', pubmsg)


def pubmsg(irc, sender, channel, message):
    t = m_thanks.search(message)
    if t:
        if irc.irclower(t.groups()[0]) == irc.irclower(irc.nickname):
            reply = 'welcome'
            try:
                reply = irc.cfg.get('thanks', irc.irclower(sender.nick))
            except:
                pass

            irc.privmsg(channel, "You're %s, %s." % (reply, sender.nick))
