import time
import datetime

plugin = None

users = {}


def init(p, bot):
    global plugin
    plugin = p
    plugin.subscribe('join', handle_join)
    plugin.subscribe('part', handle_part)
    plugin.subscribe('kick', handle_kick)
    plugin.subscribe('quit', handle_quit)
    plugin.bind_public('!seen', seen)


def seen(irc, sender, channel, args):
    global users

    u = args[1]
    if u in users:
        t = users[u][1]
        d = datetime.fromtimestamp(t)

        irc.privmsg(channel,
                    "%s, the last time I saw %s was %s." %
                    (sender.nick, u, d.strftime("%b %d, %Y %H:%M %Z")))
    else:
        if channel.get_user(u):
            irc.privmsg(channel,
                        "%s, %s is here right now!" %
                        (sender.nick, u))
        else:
            irc.privmsg(channel,
                        "Sorry, %s. I haven't seen %s before." %
                        (sender.nick, u))


def handle_part(irc, sender, channel, message):
    global users

    if sender.nick != irc.nickname:
        users[sender.nick] = ('part', time.time(), [channel, message])


def handle_kick(irc, sender, channel, nick, message):
    global users

    if nick != irc.nickname:
        users[nick] = ('kick', time.time(), [channel, message])


def handle_quit(irc, sender, message):
    global users
    if sender.nick != irc.nickname:
        users[sender.nick] = ('quit', time.time(), [message])


def handle_join(irc, sender, channel):
    global users
    if sender.nick != irc.nickname and sender.nick in users:
        del users[sender.nick]
