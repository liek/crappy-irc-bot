# logger.py
# Logs channels/privmsgs

# TODO:
# - Document format strings
# - Log roatating
# - Follow nickname changes for privmsgs?
# - Make which events are logged configurable -- Just comment out the format
###############################################################################

import time
import datetime
import re
import configparser
import string

plugin = None

cfg_file = "logger.cfg"
my_logs = {}
log_dir = "logs/"
logfmt = {}
formatter = string.Formatter()
log_channels = []
log_messages = []
strip_formatting = False

fstrip = re.compile(r"(?:\x03(?:\d{1,2}(?:,\d{1,2})?)?)|[\x02\x1F\x16\x0F]")


class LogFile():
    def __init__(self, fh=None, last_write=time.localtime()):
        self.fh = fh
        self.last_write = last_write


def init(p, bot):
    global plugin, cfg_file, log_dir, logfmt
    global log_channels, log_messages, strip_formatting
    plugin = p

    plugin.subscribe('privmsg', handle_privmsg)
    plugin.subscribe('pubmsg', handle_pubmsg)
    plugin.subscribe('notice', handle_notice)
    plugin.subscribe('pubnotice', handle_pubnotice)
    plugin.subscribe('action', handle_action)
    plugin.subscribe('pubaction', handle_pubaction)
    plugin.subscribe('ctcp', handle_ctcp)
    plugin.subscribe('ctcpreply', handle_ctcpreply)
    plugin.subscribe('join', handle_join)
    plugin.subscribe('part', handle_part)
    plugin.subscribe('mode', handle_mode)
    plugin.subscribe('quit', handle_quit)
    plugin.subscribe('kick', handle_kick)
    plugin.subscribe('topic', handle_topic)
    plugin.subscribe('nick', handle_nick)
    plugin.subscribe('privmsg_self', handle_privmsg_self)
    plugin.subscribe('notice_self', handle_notice_self)
    plugin.subscribe('action_self', handle_action_self)
    plugin.subscribe('ctcp_self', handle_ctcp_self)
    plugin.subscribe('ctcpreply_self', handle_ctcpreply_self)
    plugin.subscribe('rpl_topic', handle_rpl_topic)
    plugin.subscribe('rpl_topicsetby', handle_rpl_topicsetby)
    plugin.subscribe('disconnect', handle_disconnect)

    try:
        cfg_file = bot.cfg.get('logger', 'cfg_file')
    except configparser.Error:
        pass

    logcfg = configparser.ConfigParser()
    try:
        logcfg.read(cfg_file)
    except configparser.Error:
        raise Exception("Unable to read logger configuration")

    try:
        log_dir = logcfg.get('main', 'log_dir')
    except:
        pass

    try:
        logfmt = dict(map(
            lambda x: (x[0].upper(), x[1]), logcfg.items('formats', raw=True)))
    except configparser.Error:
        raise Exception("Unable to load log formats")

    try:
        log_channels = logcfg.get('main', 'log_channels').split()
    except configparser.Error:
        pass

    try:
        log_messages = logcfg.get('main', 'log_messages').split()
    except configparser.Error:
        pass

    try:
        strip_formatting = logcfg.getboolean('main', 'strip_formatting')
    except configparser.Error:
        pass


def unload():
    handle_disconnect(None, None)


def handle_privmsg(irc, sender, message):
    name = get_log(irc, sender.nick)
    if not name:
        return
    print_log(name, 'PRIVMSG', nick=sender.nick, message=message)


def handle_pubmsg(irc, sender, channel, message):
    t = channel.lstrip(irc.user_prefixes)
    format = 'PRIVMSG_CHAN' if t == channel else 'OMSG'  # I know...
    name = get_log(irc, t)
    if not name:
        return
    print_log(name, format, nick=sender.nick, channel=channel, message=message)


def handle_action(irc, sender, message):
    name = get_log(irc, sender.nick)
    if not name:
        return
    print_log(name, 'ACTION', nick=sender.nick, message=message)


def handle_pubaction(irc, sender, channel, message):
    t = channel.lstrip(irc.user_prefixes)
    name = get_log(irc, t)
    if not name:
        return
    print_log(name, 'ACTION_CHAN', nick=sender.nick, message=message)


def handle_notice(irc, sender, message):
    name = get_log(irc, sender.nick)
    if not name:
        return
    print_log(name, 'NOTICE', nick=sender.nick, message=message)


def handle_pubnotice(irc, sender, channel, message):
    t = channel.lstrip(irc.user_prefixes)
    name = get_log(irc, t)
    if not name:
        return
    print_log(name, 'NOTICE_CHAN', nick=sender.nick, channel=channel,
              message=message)


def handle_privmsg_self(irc, sender, target, message):
    name = ""
    chan = False
    format = ""

    t = target.lstrip(irc.user_prefixes)

    if irc.is_channel(t):
        name = t
        chan = True
        format = 'OMSG' if target != t else 'PRIVMSG_CHAN'
    else:
        name = target

    name = get_log(irc, name)

    if not name:
        return

    if chan:
        print_log(name, format, nick=sender.nick, channel=target,
                  message=message)
    else:
        print_log(name, 'PRIVMSG', nick=sender.nick, message=message)


def handle_notice_self(irc, sender, target, message):
    name = ""
    chan = False
    t = target.lstrip(irc.user_prefixes)

    if irc.is_channel(t):
        name = t
        chan = True
    else:
        name = target

    name = get_log(irc, name)

    if not name:
        return

    if chan:
        print_log(name, 'NOTICE_CHAN', nick=sender.nick, channel=target,
                  message=message)
    else:
        print_log(name, 'NOTICE', nick=sender.nick, message=message)


def handle_action_self(irc, sender, target, message):
    name = ""
    chan = False
    t = target.lstrip(irc.user_prefixes)

    if irc.is_channel(t):
        name = t
        chan = True
    else:
        name = target

    if not name:
        return

    if chan:
        print_log(name, 'ACTION_CHAN', nick=sender.nick, message=message)
    else:
        print_log(name, 'ACTION', nick=sender.nick, message=message)


def handle_ctcp(irc, sender, target, command, data):
    name = ""
    chan = False
    t = target.lstrip(irc.user_prefixes)

    if irc.is_channel(t):
        name = t
        chan = True
    else:
        name = sender.nick

    name = get_log(irc, name)

    if not name:
        return

    if chan:
        print_log(name, 'CTCP_CHAN', nick=sender.nick, channel=target,
                  command=command, data=data)
    else:
        print_log(name, 'CTCP', nick=sender.nick, command=command, data=data)


def handle_ctcpreply(irc, sender, target, command, data):
    name = ""
    chan = False
    t = target.lstrip(irc.user_prefixes)

    if irc.is_channel(t):
        name = t
        chan = True
    else:
        name = sender.nick

    name = get_log(irc, name)

    if not name:
        return

    if chan:
        print_log(name, 'CTCPREPLY_CHAN', nick=sender.nick, channel=target,
                  command=command, data=data)
    else:
        print_log(name, 'CTCPREPLY', nick=sender.nick, command=command,
                  data=data)


def handle_ctcp_self(irc, sender, target, command, data):
    t = target.lstrip(irc.user_prefixes)
    name = get_log(irc, t)
    if not name:
        return
    print_log(name, 'CTCP_SELF', target=target, command=command, data=data)


def handle_ctcpreply_self(irc, sender, target, command, data):
    pass


def handle_join(irc, sender, channel):
    name = get_log(irc, channel)
    if not name:
        return
    print_log(name, 'JOIN', nick=sender.nick, user=sender.username,
              host=sender.host, channel=channel)


def handle_part(irc, sender, channel, message):
    name = get_log(irc, channel)

    if not name:
        return

    print_log(name, 'PART', nick=sender.nick, user=sender.username,
              host=sender.host, channel=channel, message=message)
    if sender.nick == irc.nickname:
        close_log(name)
        del my_logs[name]


def handle_mode(irc, sender, channel, modes, params):
    name = get_log(irc, channel)
    if not name:
        return
    mode_str = " ".join([modes] + list(params))
    print_log(name, 'MODE_CHAN', nick=sender.nick, user=sender.username,
              host=sender.host, channel=channel, modes=mode_str)


def handle_quit(irc, sender, message):
    name = ""

    for channel in irc.chmgr.channels:
        name = get_log(irc, channel)
        if not name:
            continue
        if irc.chmgr.get(channel).get_user(sender.nick):
            print_log(name, 'QUIT', nick=sender.nick, user=sender.username,
                      host=sender.host, message=message)
            if sender.nick == irc.nickname:
                close_log(name)
                del my_logs[name]


def handle_kick(irc, sender, channel, nick, message):
    name = get_log(irc, channel)
    if not name:
        return

    if nick == irc.nickname:
        print_log(name, 'KICKME', knick=nick, channel=channel,
                  nick=sender.nick, message=message)
        close_log(name)
        del my_logs[name]
    else:
        print_log(name, 'KICK', knick=nick, channel=channel, nick=sender.nick,
                  message=message)


def handle_topic(irc, sender, channel, topic):
    name = get_log(irc, channel)
    if not name:
        return
    print_log(name, 'TOPIC', nick=sender.nick, topic=topic)


def handle_nick(irc, sender, newnick):
    for channel in irc.chmgr.channels:
        if irc.chmgr.get(channel).get_user(sender.nick):
            name = get_log(irc, channel)
            if name:
                print_log(name, 'NICK', nick=sender.nick, newnick=newnick)


def handle_rpl_topic(irc, sender, channel, topic):
    name = get_log(irc, channel)
    if not name:
        return
    print_log(name, 'TOPIC_IS', channel=channel, topic=topic)


def handle_rpl_topicsetby(irc, sender, channel, setter, when):
    name = get_log(irc, channel)
    if not name:
        return
    if not when:
        when = "0"
    date = time.strftime(logfmt['FULLDATE'], time.localtime(int(when)))
    print_log(name, 'TOPIC_SETBY', nick=setter, channel=channel, date=date)


def handle_disconnect(irc, sender, **kwargs):
    for f in my_logs:
        close_log(f)
    my_logs.clear()


def get_log(irc, target):
    logging = False
    my_list = log_messages

    if not target:
        return

    name = irc.irclower(target)

    if irc.is_channel(target):
        my_list = log_channels

    if "*" in my_list:
        logging = True
        if ("-%s" % (target)) in my_list:
            logging = False
    else:
        if target in my_list:
            logging = True

    if not logging:
        return None

    if name not in my_logs:
        fn = log_dir + name + ".log"

        try:
            my_logs[name] = LogFile(open(fn, 'a', 1, encoding='utf-8'),
                                    time.localtime())
        except IOError:
            return

        try:
            if 'SESSION_START' in logfmt:
                my_logs[name].fh.write(logfmt['SESSION_START'].format(
                                       date=time.strftime(logfmt['FULLDATE']))
                                       + "\n")
        except IOError:
            plugin.logger.info("Error writing to log file")
            return

    else:
        # Check last time we wrote to the file
        curtime = time.localtime()
        if curtime.tm_mday != my_logs[name].last_write.tm_mday:
            dt = datetime.datetime(curtime.tm_year, curtime.tm_mon,
                                   curtime.tm_mday, 0, 0, 0)
            try:
                if 'SESSION_TIME' in logfmt:
                    date = dt.strftime(logfmt['FULLDATE'])
                    my_logs[name].fh.write(logfmt['SESSION_TIME'].format(
                                           date=date) + "\n")
            except IOError:
                plugin.logger.info("Error writing to log file")
    return name


def print_log(name, fmt, **kwargs):
    if not name or name not in my_logs:
        return
    if not fmt or fmt not in logfmt:
        return

    curtime = time.localtime()

    try:
        line = formatter.vformat(logfmt[fmt], (), kwargs)
        if strip_formatting:
            line = fstrip.sub("", line)
        my_logs[name].fh.write(time.strftime(logfmt['TIMESTAMP'], curtime) +
                               " " + line + "\n")
        my_logs[name].last_write = curtime
    except IOError:
        plugin.logger.info("Error writing to log file")


def close_log(name):
    if not name or name not in my_logs:
        return

    curtime = time.localtime()

    try:
        my_logs[name].fh.write(logfmt['SESSION_CLOSE'].format(
                               date=time.strftime(logfmt['FULLDATE'], curtime))
                               + "\n")
        my_logs[name].fh.write("\n")
        my_logs[name].fh.close()
    except IOError:
        plugin.logger.info("Error writing to log file")
