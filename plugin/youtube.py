# (formerly) boof's youtube script
# Without gdata dependency (gdata doesn't run on py3) -Matt
# Now with youtube v3 api (ugh..)
import json
import re
from collections import OrderedDict
import time
import datetime
import configparser

import urllib.request as UL
from urllib.error import URLError

plugin = None

m_dur = re.compile(r'^P(?:(\d+)D)?T(?:(\d+)H)?(?:(\d+)M)?(?:(\d+)S)?')
dkeys = ['days', 'hours', 'mins', 'secs']

yt_apikey = ""


def init(p, bot):
    global plugin, yt_apikey
    plugin = p
    plugin.subscribe('pubmsg', pubmsg)
    try:
        yt_apikey = bot.cfg.get('youtube', 'apikey')
    except configparser.Error:
        pass
    plugin.logger.info("youtube plugin GO!")


def pubmsg(irc, sender, channel, message):
    yid = ""
    loc = message.find("youtu.be/")
    if loc != -1:
        loc = loc + 9
        yid = message[loc:loc + 11]
    elif message.find("v=") != -1:
        if message.find("youtube.com") != -1:
            ylen = message.find("v=") + 2
            yid = message[ylen:ylen + 11]
    if yid:
        url = ('https://www.googleapis.com/youtube/v3/videos?id=%s&key=%s&'
               'part=snippet,contentDetails,statistics,liveStreamingDetails')
        url = url % (yid, yt_apikey)
        try:
            req = UL.urlopen(url, timeout=5)
        except URLError as e:
            irc.privmsg(channel, "%s: Unable to connect to youtube: %s" %
                        (sender.nick, e))
            return
        try:
            data = req.read().decode('utf-8')
            d = json.loads(data)
            e = d['items'][0]
        except (OSError, ValueError, KeyError, IndexError) as e:
            irc.privmsg(channel,
                        "%s: Invalid response received from youtube: %s" %
                        (sender.nick, e))

        title = "(no title)"
        try:
            title = e['snippet']['title']
        except KeyError:
            pass
        # filter RtL/LtR marks
        title = title.replace('\u202b', '')
        title = title.replace('\u202a', '')

        likes = 0
        dislikes = 0
        try:
            likes = int(e['statistics']['likeCount'])
        except (KeyError, ValueError):
            pass
        try:
            dislikes = int(e['statistics']['dislikeCount'])
        except (KeyError, ValueError):
            pass

        votes = likes + dislikes
        avg = (likes / votes) * 5.0 if votes else 0

        dur = None
        try:
            isodur = e['contentDetails']['duration']
            dur = OrderedDict(zip(dkeys,
                                  [int(x or 0) for x in
                                   m_dur.match(isodur).groups()]))

        except (KeyError, AttributeError):
            dur = OrderedDict(zip(dkeys, (0, 0, 0, 0)))

        btype = 'none'
        try:
            btype = e['snippet']['liveBroadcastContent']
        except KeyError:
            pass

        viewers = "0"
        try:
            viewers = "{:,}".format(int(e['statistics']['viewCount']))
        except (KeyError, ValueError):
            pass

        msg = ""

        if btype == 'live':
            curr_viewers = "0"
            try:
                curr_viewers = "{:,}".format(
                    int(e['liveStreamingDetails']['concurrentViewers']))
            except (KeyError, ValueError):
                pass
            msg = ('%s: \037%s\037 is currently being watched by %s nokio fans '
                   "and %s the %s who've viewed it.") % (
                       sender.nick, title, curr_viewers,
                       ("is %.1f/5.0 according to" % avg if votes else
                        "has not been rated by"), viewers)
        elif btype == 'upcoming':
            try:
                s = e['liveStreamingDetails']['scheduledStartTime']
                stime = datetime.datetime(*time.strptime(
                    s, '%Y-%m-%dT%H:%M:%S.%fZ')[0:6])
                ctime = datetime.datetime.utcnow()
                if ctime > stime:
                    begins_in = "is running late"
                else:
                    bdelta = stime - datetime.datetime.utcnow()
                    mins, secs = divmod(bdelta.seconds, 60)
                    hours, mins = divmod(mins, 60)
                    start_time = OrderedDict(zip(dkeys,
                                                 [bdelta.days, hours,
                                                  mins, secs]))
                begins_in = "starts in " + duration_str(start_time)
            except (KeyError, ValueError):
                begins_in = "starts sometime"

            msg = ('%s: \037%s\037 %s and %s the %s nokio fans '
                   "that haven't watched it." % (
                       sender.nick, title, begins_in,
                       ('is %.1f/5.0 according to' % avg if votes else
                        'has not been rated by'), viewers))
        else:
            msg = ("%s: \037%s\037 %s the %s "
                   "nokio fans that watched it, and will bore us for %s." %
                   (sender.nick, title,
                    ('is %.1f/5.0 according to' % avg if votes else
                     'has not been rated by'), viewers, duration_str(dur)))
        irc.privmsg(channel, msg)


def duration_str(d):
    return ''.join([('%d %s ' % (d[x], x[:-1] if d[x] == 1 else x)
                     if d[x] else '') for x in d.keys()]).rstrip()
