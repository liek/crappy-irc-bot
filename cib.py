#!/usr/bin/env python3

import sys
import os
import time
import atexit
import signal
from signal import SIGTERM, SIGINT, SIGHUP
import getopt
import configparser
import logging

from bot import Bot

logger = logging.getLogger('cib')
logfmt = logging.Formatter('%(asctime)s %(name)s: %(message)s')

# Daemon functionality for crappy-irc-bot
# Code borrowed from
#  http://www.jejik.com/articles/2007/02/a_simple_unix_linux_daemon_in_python/


class Daemon:
    """
    A generic daemon class.
    Usage: subclass the Daemon class and override the run() method
    """
    def __init__(self, pidfile, stdin=os.devnull, stdout=os.devnull,
                 stderr=os.devnull):
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.pidfile = pidfile

    def daemonize(self):
        """
        do the UNIX double-fork magic, see Stevens' "Advanced
        Programming in the UNIX Environment" for details (ISBN 0201563177)
        http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16
        """
        try:
            pid = os.fork()
            if pid > 0:
                # exit first parent
                sys.exit(0)
        except OSError as e:
            print("fork #1 failed: %d (%s)\n" %
                  (e.errno, e.strerror))
            sys.exit(1)

        # decouple from parent environment
        #os.chdir("/")
        os.setsid()
        os.umask(0)

        # do second fork
        try:
            pid = os.fork()
            if pid > 0:
                # exit from second parent
                sys.exit(0)
        except OSError as e:
            print("fork #2 failed: %d (%s)\n" %
                  (e.errno, e.strerror))
            sys.exit(1)

        # redirect standard file descriptors
        sys.stdout.flush()
        sys.stderr.flush()
        si = open(self.stdin, 'r')
        so = open(self.stdout, 'a+')
        se = open(self.stderr, 'a+')
        os.dup2(si.fileno(), sys.stdin.fileno())
        os.dup2(so.fileno(), sys.stdout.fileno())
        os.dup2(se.fileno(), sys.stderr.fileno())

        # write pidfile
        atexit.register(self.delpid)
        pid = str(os.getpid())
        open(self.pidfile, 'w+').write("%s\n" % pid)

    def delpid(self):
        os.remove(self.pidfile)

    def read_pidfile(self):
        try:
            pf = open(self.pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except (IOError, ValueError):
            pid = None
        return pid

    def start(self):
        """
        Start the daemon
        """
        # Check for a pidfile to see if the daemon already runs
        pid = self.read_pidfile()

        if pid:
            message = "pidfile %s already exists. CIB already running?\n"
            sys.stderr.write(message % self.pidfile)
            sys.exit(1)

        # Start the daemon
        self.daemonize()
        self.run()

    def stop(self):
        """
        Stop the daemon
        """
        # Get the pid from the pidfile
        pid = self.read_pidfile()

        if not pid:
            message = "pidfile %s does not exist. CIB not running?\n"
            sys.stderr.write(message % self.pidfile)
            return  # not an error in a restart

        # Try killing the daemon process
        try:
            os.kill(pid, 0)
        except OSError:
            sys.stderr.write("Bad PID? Deleting.\n")

            if os.path.exists(self.pidfile):
                os.remove(self.pidfile)
            sys.exit(1)
        try:
            while 1:
                os.kill(pid, SIGTERM)
                time.sleep(0.1)
        except OSError as err:
            if err.errno == 3:
                if os.path.exists(self.pidfile):
                    os.remove(self.pidfile)
            else:
                print(str(err))
                sys.exit(1)

    def restart(self):
        """
        Restart the daemon
        """
        self.stop()
        self.start()

    def reload_config(self):
        """
        Reload bot configuration
        """
        pid = self.read_pidfile()

        if not pid:
            message = "pidfile %s does not exist. CIB not running?\n"
            sys.stderr.write(message % self.pidfile)
            return
        try:
            os.kill(pid, SIGHUP)
        except OSError:
            sys.stderr.write("Bad PID?\n")
            return

    def run(self):
        """
        You should override this method when you subclass Daemon.
        It will be called after the process has been daemonized by start()
        or restart().
        """


class CIBDaemon(Daemon):
    def set_stop(self, *args):
        logger.info("Recieved SIGTERM")
        if not self.shutdown:
            self.shutdown = True
            logger.info("Issuing shutdown")
            self.bot._queue.put(('shutdown', "SIGTERM received, terminating"))

    def send_reload_config(self, *args):
        logger.info("Recieved SIGHUP")
        self.bot._queue.put(('reload', "Recieved SIGHUP"))

    def init(self, cfg_file="cib.py", cfg=None):
        self.cfg_file = cfg_file
        self.cfg = cfg
        #self.logger._defaultFormatter = logging.Formatter(u"%(message)s")
        rlog = logging.getLogger()
        rlog.setLevel(logging.DEBUG)

    def add_file_log_handler(self, level, filename):
        rlog = logging.getLogger()
        ch = logging.FileHandler(filename, encoding='utf-8')
        ch.setLevel(level)
        ch.addFilter(MyLogFilter(level))
        ch.setFormatter(logfmt)
        rlog.addHandler(ch)

    def add_stream_log_handler(self, level, fh):
        rlog = logging.getLogger()
        ch = logging.StreamHandler(fh)
        ch.setLevel(level)
        ch.addFilter(MyLogFilter(level))
        rlog.addHandler(ch)

    def run(self):
        self.shutdown = False

        main_file = "log/main.log"
        try:
            main_file = cfg.get('main', 'log_main')
        except configparser.Error:
            pass

        error_file = "log/error.log"
        try:
            error_file = cfg.get('error', 'log_error')
        except configparser.Error:
            pass

        debug_file = "log/debug.log"
        try:
            debug_file = cfg.get('main', 'debug_file')
        except configparser.Error:
            pass

        self.add_file_log_handler(logging.INFO, main_file)
        self.add_file_log_handler(logging.ERROR, error_file)
        self.add_file_log_handler(logging.DEBUG, debug_file)

        signal.signal(SIGTERM, self.set_stop)
        signal.signal(SIGINT, self.set_stop)
        signal.signal(SIGHUP, self.send_reload_config)

        bot = Bot(cfg_file=self.cfg_file, cfg=self.cfg)
        logger.info("CIB Starting")
        self.bot = bot
        bot.start()
        while bot.is_alive():
            bot.join(1)           # Without a timeout the process will deadlock
                                  # and never execute the signal handler
        bot._conn.disconnect()    # Make sure we kill the socket threads
        bot.pmgr.shutdown_plugins()
        logger.info("Bot Exiting")
        sys.exit(0)


class MyLogFilter():
    """Filters log messages to only the specified level"""
    def __init__(self, level):
        self.level = level

    def filter(self, record):
        return (self.level == record.levelno)


if __name__ == '__main__':
    main_log = "log/main.log"
    error_log = "log/error.log"
    pid_file = "cib.pid"
    version = "crappy-irc-bot"
    cfg_file = 'cib.cfg'

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'c:', ['config-file='])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-c', '--config-file'):
            cfg_file = arg

    cfg = configparser.ConfigParser(allow_no_value=True)
    try:
        cfg.read(cfg_file)
    except configparser.Error:
        print("Error loading config file")
        exit(1)

    error_file = "log/error.log"
    try:
        error_file = cfg.get('error', 'log_error')
    except configparser.Error:
        pass

    try:
        pid_file = cfg.get('main', 'pid_file')
    except configparser.Error:
        pass

    try:
        version = cfg.get('main', 'version')
    except configparser.Error:
        pass

    cib = CIBDaemon(pid_file, stderr=error_log)
    cib.init(cfg_file, cfg)

    if len(args) >= 1:
        if args[0] == 'fg' or args[0] == 'nofork':
            # Setup logging to console
            cib.add_stream_log_handler(logging.INFO, sys.stdout)
            cib.add_stream_log_handler(logging.ERROR, sys.stderr)
            cib.add_stream_log_handler(logging.DEBUG, sys.stdout)
            cib.run()
        else:
            if args[0] == 'stop':
                cib.stop()
            elif args[0] == 'restart':
                cib.restart()
            elif args[0] == 'reload':
                cib.reload_config()
            else:
                print(version + " Starting")
                cib.start()
    else:
        print(version + " Starting")
        cib.start()
