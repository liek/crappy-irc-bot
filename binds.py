import logging

logger = logging.getLogger(__name__)


class Bind(object):
    def __init__(self, plugin, func, split, maxsplits):
        self.plugin = plugin
        self.func = func
        self.split = split
        self.maxsplits = maxsplits

    def call(self, bot, trigger, data, kwargs):
        if self.split:
            kwargs['args'] = [trigger] + data.split(None, self.maxsplits)
        else:
            kwargs['args'] = [trigger, data]
        kwargs['func'] = self.func
        self.plugin.add('bind', bot, kwargs)


class Binds(object):
    def __init__(self):
        self._binds = {}
        self.events = {'pubmsg': 'message',
                       'privmsg': 'message',
                       }
        for e in self.events:
            self._binds[e] = {}

    def add(self, event, plugin, trigger, func, split=True, maxsplits=-1):
        if event not in self.events:
            raise Exception('Invalid event: %s' % (event))
        if not trigger:
            raise Exception('Invalid trigger!')
        if trigger in self._binds[event]:
            raise Exception('Trigger %s was already defined for %s!' %
                            (trigger, event))
        self._binds[event][trigger] = Bind(plugin, func, split, maxsplits)

    def clear(self, plugin):
        for e in self._binds:
            for t in list(self._binds[e].keys()):
                if self._binds[e][t].plugin == plugin:
                    del self._binds[e][t]

    def call(self, event, bot, kwargs):
        try:
            trigger, data = kwargs[self.events[event]].partition(' ')[::2]
        except Exception as e:
            logger.info("Bind Error (Invalid Event?): %s" % (e))
            return
        if not trigger or trigger not in self._binds[event]:
            return
        call_kwargs = {'sender': kwargs['sender']}
        if 'channel' in kwargs:
            call_kwargs['channel'] = bot.chmgr.get(kwargs['channel'])
        self._binds[event][trigger].call(bot, trigger, data, call_kwargs)
