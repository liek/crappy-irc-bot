def encode(text):
    """Tries to encode a string as UTF-8, falls back to windows-1252 on
    failure

    Parameters:
    text - String to encode

    Returns the encoded string
    """
    try:
        text = text.encode('utf-8')
    except:
        text = text.encode('windows-1252', 'replace')

    return text


def decode(text):
    """Tries to decode a string as UTF-8. Falls back to windows-1252 on
    failure

    Parameters:
    text = String to decode

    Returns the decoded string
    """
    try:
        text = text.decode('utf-8')
    except:
        text = text.decode('windows-1252', 'replace')

    return text
